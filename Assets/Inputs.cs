﻿using UnityEngine;
using System.Collections;

using System.IO;
using System;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class Inputs : MonoBehaviour
{

    static Dictionary<string, bool> dic = new Dictionary<string, bool>();
    static List<string> keyList;

    static public Vector3 move;
    static public Vector3 select;   //メニュー操作用
    static public Vector3 cameramove;
    static public bool useController;
    static public bool Jump;
    static public bool Atack;
    static public bool Dash;
    static public bool Shot;
    static public bool Guard;

    static public bool[] mouseButton = new bool[3];
    static Vector3 before_mousePos;
    static Vector3 mousePos;
    static Vector3 mouseMove;

    static bool initialized;

    static public Vector3 GetMousePosition()
    {
        return mousePos;
    }

    static public bool getKey(string keyName)
    {

        return dic[keyName];

    }

    // Use this for initialization
    void Start()
    {
        if (!initialized)
        {
            dic.Add("k_W", false);
            dic.Add("k_A", false);
            dic.Add("k_S", false);
            dic.Add("k_D", false);
            dic.Add("k_SPACE", false);
            dic.Add("k_SHIFT", false);
            keyList = new List<string>(dic.Keys);

            Cursor.lockState = CursorLockMode.Locked;
            initialized = true;
        }
    }

    // Update is called once per frame
    void Update()
    {

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;


        CheckController();

        //Debug.Log(mouseMove);

        foreach (string key in keyList)
        {
            dic[key] = Input.GetButton(key);
        }

        for (int i = 0; i < 3; i++)
        {
            mouseButton[i] = Input.GetMouseButton(i);
        }
        mousePos = Input.mousePosition;
        mouseMove = new Vector2(Input.GetAxisRaw("MouseX"), Input.GetAxisRaw("MouseY"));

        if (useController)
        {
            move.x = Input.GetAxis("Horizontal");
            move.z = Input.GetAxis("Vertical");
            cameramove = new Vector2(Input.GetAxis("R_Horizontal") * 3, Input.GetAxis("R_Vertical") * 3);
            Jump = Input.GetButton("Jump");
            Atack = Input.GetButton("Cancel");
            Dash = Input.GetButton("Dash");
            Shot = Input.GetAxis("Shot") < 0.0f;
            Guard = Input.GetButton("Guard");
        }
        else
        {
            move = Vector3.zero;
            if (dic["k_W"])
            {
                move.z += 1;

            }
            if (dic["k_S"])
            {
                move.z -= 1;

            }
            if (dic["k_D"])
            {
                move.x += 1;

            }
            if (dic["k_A"])
            {
                move.x -= 1;

            }
            cameramove = mouseMove;
            Jump = dic["k_SPACE"];
            Atack = mouseButton[1];
            Dash = dic["k_SHIFT"];
            Shot = mouseButton[0];
            Guard = mouseButton[2];
        }
        if (move.magnitude > 1.0f)
        {
            move.Normalize();
        }
        before_mousePos = mousePos;
    }

    void CheckController()
    {
        if (Input.GetJoystickNames().Length == 0)
        {
            useController = false;
            return;
        }
        else
        {
            useController = true;
        }
    }
}
