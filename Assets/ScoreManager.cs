﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

using NCMB;
using System.Collections.Generic;

public class ScoreManager : MonoBehaviour
{

    static int m_Score;

    Text text;
    bool isSend;

    bool isOnline;

    bool isEndOnline;
    bool isEndScoreSort;
    NCMBObject testClass;

    int[] OnlineScore;
    string[] OnlineUserName;

    List<Text> RankMenuText;
    List<Text> RankMenuNameText;
    string songHash;
    Text songtext;

    static string userName;

    bool[] data;

    public static int Score
    {
        set
        {
            m_Score = value;
        }
        get
        {
            return m_Score;
        }
    }

    public static void AddScore(int add)
    {
        if (MusicController.inMusic)
            m_Score += add;
        else
        {
            m_Score += 20;
        }
    }
    public static void SubScore(int sub)
    {
        if (MusicController.inMusic)
            m_Score -= sub;
        else
        {
            m_Score -= 100;
        }

    }

    void OnLevelWasLoaded()
    {
        if (SceneManager.GetActiveScene().buildIndex == 0)
        {
            isOnline = false;
            isEndOnline = false;
            isEndScoreSort = false;
            m_Score = 0;
            songHash = "";
        }
        else if (SceneManager.GetActiveScene().buildIndex == 1 || SceneManager.GetActiveScene().buildIndex == 2)
        {
            text = GameObject.Find("ScoreText").GetComponent<Text>();
        }
        else if (SceneManager.GetActiveScene().buildIndex == 3)
        {
            RankMenuText = new List<Text>();
            RankMenuNameText = new List<Text>();
            GameObject rank = GameObject.Find("Rank");

            foreach (Transform T in rank.transform)
            {
                if (T.name == "Image")
                {
                    RankMenuText.Add(T.GetChild(0).GetComponent<Text>());
                    RankMenuNameText.Add(T.GetChild(0).GetChild(0).GetComponent<Text>());
                }
                else if (T.name == "SongText")
                {
                    songtext = T.GetComponent<Text>();
                    songtext.text = MusicController.Property.title + '\n' + MusicController.Property.artist;
                }
            }


            NetworkCheck();
            if (isOnline)
            {
                songHash = "ID" + MusicController.Count3.ToString() + MusicController.Count4.ToString() + MusicController.allSamplesCount.ToString();
                OnlineScore = new int[4];
                OnlineUserName = new string[4];
                data = new bool[4];
                GetRank();
                SetRank();
                
            }
        }
    }

    
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {


        Debug.Log(userName);
    }

    void NetworkCheck()
    {
        switch (Application.internetReachability)
        {
            case NetworkReachability.NotReachable:
                isOnline = false;
                break;
            //ネットワークに接続可能な場合の処理
            default:
                isOnline = true;
            break;
        }
    }

    void GetRank()
    {
        NCMBQuery<NCMBObject> query = new NCMBQuery<NCMBObject>(songHash);
        query.WhereNotEqualTo("testScore", "");//messageが入力されていないもの以外全て取得

        //データを検索し取得
        query.FindAsync((List<NCMBObject> objectList, NCMBException e) =>
        {

            //取得失敗
            if (e != null)
            {
                //エラーコード表示
                Debug.Log(e.ToString());
                isOnline = false;
                return;
            }

            int[] tempRank = new int[3];
            bool[] tempdata = new bool[3];

            string[] tempRankName = new string[3];
            //取得した全データのmessageを表示
            foreach (NCMBObject ncbObject in objectList)
            {
                string scoreText = ncbObject["testScore"].ToString();
                 string userName;
                try
                {
                    userName = ncbObject["userName"].ToString();
                }
                catch { userName = "アノニマス"; };
                int tempScore = int.Parse(scoreText);
                string tempName = userName;
                for (int i = 0; i < tempRank.Length; i++)
                {
                    if (tempRank[i] < tempScore || !tempdata[i])
                    {
                        int a = tempRank[i];
                        tempRank[i] = tempScore;
                        tempScore = a;
                        string b = tempRankName[i];
                        tempRankName[i] = tempName; 
                        tempName = b;
                        tempdata[i] = true;
                    }
                }
            }
            for (int i = 0; i < tempRank.Length; i++ )
            {
                OnlineScore[i] = tempRank[i];
                OnlineUserName[i] = tempRankName[i];
                data[i] = tempdata[i];
            }
            isEndOnline = true;
        });
    }

    void SetRank()
    {
        StartCoroutine(SetRankCoroutine());
    }

    void ShowRank()
    {
        if (isEndOnline)
        {
            if (!isEndScoreSort)
            {
                int tempScore;
                string tempName;
                tempScore = m_Score;
                tempName = userName;
                bool tempdata = true; //ユーザー
                
                for (int i = 0; i < OnlineScore.Length; i++)
                {
                    if (OnlineScore[i] < tempScore)
                    {
                        int a = OnlineScore[i];
                        OnlineScore[i] = tempScore;
                        tempScore = a;
                        string b = OnlineUserName[i];
                        OnlineUserName[i] = tempName;
                        tempName = b;
                        bool c = data[i];
                        data[i] = tempdata;
                        tempdata = c;
                    }
                    if (i == OnlineScore.Length -1 && OnlineScore[OnlineScore.Length - 1] > tempScore && tempScore == m_Score)
                    {

                        OnlineScore[OnlineScore.Length - 1] = tempScore;
                        OnlineUserName[OnlineUserName.Length - 1] = tempName;
                        data[i] = true;
                    }
                    
                }
                isEndScoreSort = true;
            }
            for (int i = 0; i < RankMenuText.Count; i++) 
            {
                if (data[i])
                {
                    RankMenuText[i].text = OnlineScore[i].ToString();
                    if (OnlineUserName[i].ToString() == "")
                        OnlineUserName[i] = "アノニマス";
                    try
                    {
                        RankMenuNameText[i].text = OnlineUserName[i].ToString();
                    }
                    catch
                    {
                        RankMenuNameText[i].text = "アノニマス";
                    }
                }
            }

        }
    }

    void OnGUI()
    {

        if (SceneManager.GetActiveScene().buildIndex == 1 || SceneManager.GetActiveScene().buildIndex == 2)
        {
            text.text = m_Score.ToString();
        }
        else if (SceneManager.GetActiveScene().buildIndex == 3)
        {
            ShowRank();
        }
    }

    IEnumerator SetRankCoroutine()
    {
        while(!isEndOnline){
            yield return 0;
        }
        testClass = new NCMBObject(songHash);

        testClass["artist"] = MusicController.Property.artist;
        testClass["title"] = MusicController.Property.title;
        testClass["testScore"] = m_Score;
        testClass["userName"] = userName;

        testClass.SaveAsync();

        isSend = true;
    }

    public static void SetUserName(string name)
    {

        userName = name;
        
    }
    public static string GetUserName()
    {
        if (userName != null)
            return userName;
        else
            return "";
    }


}
