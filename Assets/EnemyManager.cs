﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyManager : MonoBehaviour
{

    public static bool timing;
    public static bool timing_hard;

    static float tempo_withtiming;
    static float tempo;
    const float animationbase = 0.5f;
    const float animationBPMrate = 0.001f;
    const float moveBase = 30;
    const float moveBPMrate = 0.05f;
    //敵スポーンテスト
    static GameObject slimePrefab;
    static GameObject torolPrefab;
    static GameObject kangaruPrefab;
    static GameObject toriPrefab;
    static GameObject[] EnemyPrefab;
    public static List<Transform> enemyList = new List<Transform>();
    static List<Vector3> spawnPos;
    // Use this for initialization
    void Start()
    {
        slimePrefab = Resources.Load<GameObject>("Enemy");
        torolPrefab = Resources.Load<GameObject>("Torol");
        kangaruPrefab = Resources.Load<GameObject>("Kangaru");
        toriPrefab = Resources.Load<GameObject>("Tori");
        EnemyPrefab = new GameObject[4]{slimePrefab,torolPrefab,kangaruPrefab,toriPrefab};
        SetSpawnPos();


    }
    void SetSpawnPos()
    {
        spawnPos = new List<Vector3>(transform.childCount);
        foreach (Transform child in transform)
        {
            spawnPos.Add(child.position);
        }
    }
    // Update is called once per frame
    void Update()
    {
        
        for (int i = 0; i < enemyList.Count; i++)
        {
            if (enemyList[i] == null)
            {
                enemyList.Remove(enemyList[i]);
                continue;
            }
           

        }
    }
    void FixedUpdate()
    {

        tempo = MusicController.GetBPM(false);

        if(0  < MusicController.GetBPM(true)){
            tempo_withtiming = MusicController.GetBPM(true);
        }
        else
        {
            tempo_withtiming -= 100;
            if (tempo_withtiming < 0)
            {
                tempo_withtiming = 0;
            }
        }

        for (int i = 0; i < enemyList.Count; i++)
        {
            if (enemyList[i] != null)
            {
                //if (enemyList[i].GetComponent<EnemyTestScript>().Type == EnemyTestScript.EnemyType.Slime)
                //{
                    enemyList[i].GetComponent<EnemyTestScript>().animation_speed = animationbase + tempo_withtiming * animationBPMrate;
                    enemyList[i].GetComponent<EnemyTestScript>().move_speed = moveBase + tempo_withtiming * moveBPMrate;
                //}else
                //{
                //    enemyList[i].GetComponent<EnemyTestScript>().animation_speed = animationbase + tempo * animationBPMrate;
                //    enemyList[i].GetComponent<EnemyTestScript>().move_speed = moveBase + tempo * moveBPMrate;
                //}
            }
        }
    }
    static public void SetEnemy(Transform t_enemy)
    {
        enemyList.Add(t_enemy);
    }
    static public void SubEnemy(Transform t_enemy)
    {
        enemyList.Remove(t_enemy);
    }
    static public void SetTempo(float nowtempo)
    {
        tempo_withtiming = nowtempo;
        SetEnemyTempo();
    }
    static void SetEnemyTempo()
    {

        foreach (Transform enemy in enemyList)
        {
            if (enemy == null)
            {
                enemyList.Remove(enemy);
                continue;
            }

        }
    }

    static public void SpawnEnemy(EnemyTestScript.EnemyType type = EnemyTestScript.EnemyType.Slime)
    {
        GameObject enemy = GameObject.Instantiate(EnemyPrefab[(int)type]);
        enemy.transform.position = spawnPos[Random.Range(0, spawnPos.Count)];

    }

    static public int GetCount()
    {
        return enemyList.Count;
    }
}
