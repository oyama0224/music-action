﻿using UnityEngine;
using System.Collections;

public class WeaponControll : MonoBehaviour
{
    GameObject atackEffect;
    GameObject effect;
    ParticleSystem effectsys;
    GameObject WeaponObj;
    Transform WeaponHand;
    GameObject bulletPrefab;
	GameObject GuardObj;
    Vector3 defGuardPos;
    public bool isAtack;
    public bool isGuard;
    Animator animator;
    public bool shot;
    const float baseSpeed = 100.0f;
    const float baseSize = 1.0f;
    public float count;
    Transform leftHand;
    Vector3 baseHandAngle;
    GameObject target;
    GameObject atackEffect2;
    // Use this for initialization
    void Start()
    {
        //atackEffect = Resources.Load<GameObject>("Effect/AtackEffect");
		animator = GetComponent<Animator> ();
		WeaponObj = GameObject.FindGameObjectWithTag("Weapon");
        atackEffect = Resources.Load<GameObject>("OnpuEffect3");
        effect = GameObject.Instantiate(atackEffect);
        effectsys = effect.GetComponent<ParticleSystem>();
        effect.GetComponent<AutoDestroy>().enabled = false;
        effect.transform.position = WeaponObj.transform.position;
        effect.transform.parent = WeaponObj.transform;
        //effect.SetActive(false);
		//GuardObj = GameObject.Find ("Guard");
        //defGuardPos = GuardObj.transform.localPosition;
        bulletPrefab = Resources.Load<GameObject>("bullet");
        //WeaponHand = WeaponObj.transform.FindChild("ken_hand");
        leftHand = transform.FindChild("sowd:pCylinder2");
        baseHandAngle = leftHand.transform.eulerAngles;
        target = GameObject.FindGameObjectWithTag("Target");
    }

    // Update is called once per frame
    void Update()
    {
        //if (Inputs.mouseButton[1] && !isAtack)
        //{
        //    //isAtack = true;
        //    //StartCoroutine(AtackCoroutine());
        //}
        //if (Inputs.mouseButton[2] && !isGuard)
        //{
        //    //isGuard = true;
        //    //GuardObj.transform.localPosition += 1.0f * Vector3.forward;
        //}
        //if(!Input.GetMouseButton(2) && isGuard)
        //{
        //    //isGuard = false;
        //    //GuardObj.transform.localPosition = defGuardPos;
        //}

        if (!isAtack)
        {
            effectsys.Play();
        }
        else
        {
        }


        if (Inputs.Shot)
        {
            target.SetActive(true);
        }
        else
        {
            target.SetActive(false);
        }

        count = count + 1 % 2;

    }
    public void Shot()
    {
        if (shot)
        {
            
            GameObject b = GameObject.Instantiate(bulletPrefab);
            b.transform.GetChild(0).GetComponent<Renderer>().material.SetFloat("_Size", baseSize + MusicController.GetVolume() / 200.0f);
            b.transform.rotation = Camera.main.transform.rotation;
            //b.transform.Rotate(leftHand.transform.localRotation.eulerAngles - baseHandAngle,Space.Self);
            b.transform.Rotate(new Vector3(0, 1, 0), count * 180.0f);
            b.transform.position = leftHand.position;
            b.GetComponent<Rigidbody>().AddForce((Camera.main.transform.TransformDirection(Vector3.forward) + transform.TransformDirection(Vector3.left) * 0.03f) *(baseSpeed + MusicController.GetBPM() /2));
        }
    }

    //IEnumerator AtackCoroutine()
    //{
    //    float count = 0;
    //    const float countLimit = 0.25f;
    //    const float angleLimit = 90.0f;
    //    int random = Random.Range(0, 3);
    //    Vector3[] direction = new Vector3[3] { Vector3.right + Vector3.forward, Vector3.right, Vector3.right + Vector3.back };
    //    while(count < countLimit){
    //        WeaponObj.transform.rotation = Quaternion.AngleAxis(count / countLimit * angleLimit, transform.TransformDirection(direction[random]));
    //       // WeaponObj.transform.Rotate( transform.TransformDirection(Vector3.right), count / 0.5f *45,Space.World );
    //        count += Time.deltaTime;
    //        yield return 0;

    //    }

    //    GameObject effect = GameObject.Instantiate(atackEffect);

    //    while (count > 0.0f)
    //    {
    //        effect.transform.position = WeaponObj.transform.position + transform.TransformDirection(Vector3.forward);
    //        WeaponObj.transform.rotation = Quaternion.AngleAxis(count / countLimit* angleLimit, transform.TransformDirection(direction[random]));
    //       // WeaponObj.transform.Rotate(transform.TransformDirection(Vector3.right), count / 0.5f * 45, Space.World);
    //        count -= Time.deltaTime;
    //        yield return 0;

    //    }
    //    WeaponObj.transform.rotation = Quaternion.AngleAxis(0,Vector3.forward);
    //    isAtack = false;
    //}
    //void OnAnimatorIK(){
    //    animator.SetIKPositionWeight(AvatarIKGoal.RightHand, 1);
    //    animator.SetIKRotationWeight(AvatarIKGoal.RightHand, 1);
    //    animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, 1);
    //    animator.SetIKRotationWeight(AvatarIKGoal.LeftHand, 1);
    //    animator.SetIKPosition(AvatarIKGoal.RightHand, WeaponHand.position);
    //    animator.SetIKRotation(AvatarIKGoal.RightHand, WeaponHand.rotation);
    //    animator.SetIKPosition(AvatarIKGoal.LeftHand, GuardObj.transform.position);
    //    animator.SetIKRotation(AvatarIKGoal.LeftHand, GuardObj.transform.rotation);



    //}
}
