﻿using UnityEngine;
using System.Collections;

public class ResultMenu : MonoBehaviour {
    GameObject gamesystem;
	// Use this for initialization
	void Start () {
        gamesystem = GameObject.Find("GameSystem");
	}
	
	// Update is called once per frame
	void Update () {
        if (FadeScript.isFade)
            return;

        if (Inputs.Jump)
        {
            MusicController.Property = new MusicController.FileProperty();
            gamesystem.GetComponent<FadeScript>().StartFade(0, "");

        }
	}
}
