﻿Shader "Unlit/BulletShader"
{
	Properties
	{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Albedo", 2D) = "white" {}
		_Emission("Color", Color) = (0,0,0,1)
		_Normal("Normal Map", 2D) = "white" { }
		_Cube("Cubemap", CUBE) = "" {}
		_Size("Value", Float) = 1.0
	}
		SubShader
		{
			Tags { "RenderType" = "Opaque" }
			LOD 200

		//	Pass
		//	{
		//		CGPROGRAM
		//		#pragma vertex vert
		//		#pragma fragment frag

		//		
		//		// make fog work
		//		#pragma multi_compile_fog


		//		uniform float _Size;

		//		#include "UnityCG.cginc"

		//		struct appdata
		//		{
		//			float4 vertex : POSITION;
		//			float2 uv : TEXCOORD0;
		//		};

		//		struct v2f
		//		{
		//			float2 uv : TEXCOORD0;
		//			UNITY_FOG_COORDS(1)
		//			float4 vertex : SV_POSITION;
		//		};

		//		sampler2D _MainTex;
		//		float4 _MainTex_ST;

		//		v2f vert(appdata v)
		//		{
		//			v2f o;

		//			o.vertex = mul(UNITY_MATRIX_MVP, v.vertex * _Size);
		//			o.uv = TRANSFORM_TEX(v.uv, _MainTex);
		//			UNITY_TRANSFER_FOG(o,o.vertex);
		//			return o;
		//		}
		//		fixed4 frag(v2f i) : SV_Target
		//		{
		//			// sample the texture
		//			fixed4 col = fixed4(_Size / 100.0f,0.0f,0.0f,1.0f);
		//		// apply fog
		//		UNITY_APPLY_FOG(i.fogCoord, col);
		//		return col;
		//	}
		//	ENDCG
		//}

			CGPROGRAM
			#pragma surface surf Lambert
			#pragma target 3.0

			struct Input {
			//float2 uv_MainTex;
			float4 color: COLOR;
			float4 emmision: COLOR;

		};


		uniform float4 _Color;
		float4 _Emission;



		void surf(Input IN, inout SurfaceOutput o) {
			o.Albedo = _Color;
			o.Emission = _Color * 5;
		}
	
		half4 LightingSimpleLambert(SurfaceOutput s, half3 lightDir, half atten)
		{
				half NdotL = max(0, dot(s.Normal, lightDir));
				half4 c;
				c.rgb = s.Albedo * _LightColor0.rgb * NdotL + fixed4(0.5f, 0.5f, 0.5f, 1);
				c.a = s.Alpha;
				return c;
			}
			ENDCG
	}
	FallBack "Diffuse"
}
