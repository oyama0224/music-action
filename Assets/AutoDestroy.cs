﻿using UnityEngine;
using System.Collections;

public class AutoDestroy : MonoBehaviour {
    public bool isEffect;

    public float Duration;
    float count = 0.0f;
#if isEffect
    
    ParticleSystem particle;
#endif
    public float destroyTime = 0.5f;
	// Use this for initialization
	void Start () {
        Destroy(gameObject,destroyTime);
#if isEffect
        particle = GetComponent<ParticleSystem>();
        particle.duration = duration;
        particle.loop = false;
#endif
    }

    // Update is called once per frame
    void Update () {

#if isEffect
        count += Time.deltaTime;
        if(Duration < count)
        {
            particle.Stop();
        }
#endif
	}
}
