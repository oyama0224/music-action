﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour
{
    GameObject player;
    Vector3 offset = new Vector3(0, 6, -7);
    float m_addAngleX;
    float angleRate = 1.0f;
    float sideRotation;
    float baseAngle;

    public bool resetCamera;
    public bool end_reset;

    public bool cameraBack;

    public int state = 0;

    float resetCount;

    public float rebase_angle;

    PlayerControll pc;

    public bool MoveCamera_under;
    public bool MoveCamer_side;
    public bool waitUnder;
    float waitUnderTime;
    bool isReturn;

    bool isDamageSet;
    float damageAngle;

    public void AddAngleX(float x)
    {
        float temp;
        temp = x * Time.deltaTime;

        if (m_addAngleX + temp > -20 && m_addAngleX + temp < 160)
            m_addAngleX += temp;

    }
    public void AddAngleY(float anglebase)
    {
        sideRotation += anglebase * Time.deltaTime;

    }
    // Use this for initialization
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        pc = player.GetComponent<PlayerControll>();
        transform.eulerAngles = player.transform.eulerAngles;
        transform.position = player.transform.position + player.transform.TransformDirection(offset);
    }

    // Update is called once per frame
    void Update()
    {
        //if (player.GetComponent<PlayerControll>().Move.z >= 0)
        //{
        //    transform.position = Vector3.Lerp(transform.position, player.transform.position + player.transform.TransformDirection(offset) + new Vector3(0, m_addAngleX / 180 * 50.0f), Time.deltaTime * 20);
        //}
        //else
        //{
        //    transform.position = Vector3.Lerp(transform.position, player.transform.position + player.transform.TransformDirection(offset) + new Vector3(0, m_addAngleX / 180 * 50.0f), Time.deltaTime * 40);
        //}

        if (pc.isReturn)
            isReturn = true;
        Vector3 p = Camera.main.WorldToViewportPoint(player.transform.position);
        Vector3 dif = Camera.main.transform.InverseTransformDirection(player.transform.position);
        MoveCamera_under = (p.y) < -0.4f;
        MoveCamer_side = (Mathf.Atan2(dif.z,dif.x) < 1.0f ) || Mathf.Abs(p.x) > 0.4f;

        //if ((!pc.isMove && !pc.isDamage && isReturn && MoveCamera) && (!end_reset && !resetCamera))
        //{
        //    isReturn = false;
        //    resetCamera = true;
        //}
        //else
        //{
        //    end_reset = false;
        if (pc.isMove)
        {
            resetCamera = false;
            end_reset = false;
        }
        //}

        transform.Rotate(new Vector3(m_addAngleX, sideRotation));
        if (transform.eulerAngles.z != 0)
        {
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, 0);
        }

        if (pc.isDamage)
        {
            float dis = Vector3.Distance(transform.position, player.transform.position);
            Vector3 space =  Camera.main.transform.InverseTransformDirection(player.transform.position);

            transform.position = Vector3.Lerp(transform.position, player.transform.position + transform.TransformDirection(offset + new Vector3(0,-2,0) * 2), 6.0f * Time.deltaTime);


            
        }
        else
        {

            Quaternion cam_rotation = transform.rotation;
            Quaternion p_rotation = Quaternion.Euler(player.transform.eulerAngles + new Vector3(m_addAngleX, sideRotation));
            //if (resetCamera)
            //{
            //    Debug.Log(pc.isMove);


            //    transform.position = Vector3.Lerp(transform.position, player.transform.position + player.transform.TransformDirection(offset), 8.0f * Time.deltaTime);
            //    Quaternion temp = p_rotation;
            //    temp.x = 0;
            //    temp.z = 0;
            //    temp = Quaternion.Lerp(cam_rotation, temp, 15.0f * Time.deltaTime);
            //    transform.rotation = temp;
            //    transform.eulerAngles = new Vector3(cam_rotation.eulerAngles.x, transform.eulerAngles.y, cam_rotation.eulerAngles.z);

            //    if (player.transform.eulerAngles.y == transform.eulerAngles.y || sideRotation > 1)
            //    {
            //        resetCamera = false;
            //    }
            //    end_reset = true;
            //    state = 0;

            //}
            //else
            //{

                float dis = Vector3.Distance(transform.position, player.transform.position);
                Vector3 space = player.transform.position - transform.position;

                if (dis > offset.magnitude && space.magnitude > offset.magnitude)
                {
                    transform.position = Vector3.Lerp(transform.position, player.transform.position + Camera.main.transform.TransformDirection(new Vector3(0, 6, -4)), 4.5f * Time.deltaTime);
                    cameraBack = true;
                    state = 1;
                }
                //else if (pc.isMove && dis < 1.0f)
                //{
                //    transform.position = Vector3.Lerp(transform.position, player.transform.position + Camera.main.transform.TransformDirection(offset), 4.5f * Time.deltaTime);
                //    cameraBack = false;
                //}
                else
                {

                    if (pc.isMove && (MoveCamera_under))
                    {

                        {
                            cameraBack = true;
                            transform.position = Vector3.Lerp(transform.position, player.transform.position + Camera.main.transform.TransformDirection(new Vector3(0, 6, -12)), 7.5f * Time.deltaTime);
                            state = 2;
                        }
                    }

                    if (!pc.isMove)
                    {
                        if (isReturn)
                        {
                            if (!MoveCamera_under)
                            {
                                waitUnderTime = 0.0f;
                                waitUnder = true;
                            }
                        }
                        if (waitUnder)
                        {

                            if (waitUnderTime < 0.2f)
                            {
                                waitUnderTime += Time.deltaTime;
                            }
                            else
                            {
                                isReturn = false;
                                waitUnder = false;
                            }
                        }

                        if ((sideRotation != 0 || MoveCamer_side || MoveCamera_under))
                        {
                            //Debug.Log("modoshi");
                            transform.position = Vector3.Lerp(transform.position, player.transform.position + transform.rotation * offset, 6.0f * Time.deltaTime);
                            //transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(new Vector3(cam_rotation.eulerAngles.x, player.transform.eulerAngles.y, cam_rotation.eulerAngles.z)),3.0f * Time.deltaTime);
                        }
                    }

                }

            }

        //}
        m_addAngleX = 0.0f;

        if (Mathf.Abs(sideRotation) > 0.001f)
            sideRotation -= sideRotation * 4.0f * Time.deltaTime;
        else
            sideRotation = 0.0f;

    }
}
