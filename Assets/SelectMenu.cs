﻿using UnityEngine;
using System.IO;
using System.Linq;
using System.Threading;


using NAudio.CoreAudioApi;
using NAudio.Wave;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Runtime.InteropServices;
using OFDDLL;
using System.Text;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

//using MMFrame.Text.Encoding;

class CharCodeDetector
{
    //文字コードの種類
    enum CharCode
    {
        ASCII,
        BINARY,
        EUC,
        JIS,
        SJIS,
        UTF16BE,
        UTF16LE,
        UTF8N
    }

    /// <summary>
    /// 読み込んでいるbyte配列内容のエンコーディングを自前で判定する
    /// </summary>
    /// <param name="data">ファイルから読み込んだバイトデータ</param>
    /// <param name="datasize">バイトデータのサイズ</param>
    /// <returns>エンコーディングの種類</returns>
    public static Encoding detectCharCode(byte[] data, int datasize)
    {
        //バイトデータ（読み取り結果）
        byte b1 = (datasize > 0) ? data[0] : (byte)0;
        byte b2 = (datasize > 1) ? data[1] : (byte)0;
        byte b3 = (datasize > 2) ? data[2] : (byte)0;
        byte b4 = (datasize > 3) ? data[3] : (byte)0;

        //UTF16Nの判定(ただし半角英数文字の場合のみ検出可能)
        if (b1 == 0x00 && (datasize % 2 == 0))
        {
            for (int i = 0; i < datasize; i = i + 2)
            {
                if (data[i] != 0x00 || data[i + 1] < 0x06 || data[i + 1] >= 0x7f)
                {   //半角OnlyのUTF16でもなさそうなのでバイナリ
                    return null;
                }
            }
            return Encoding.BigEndianUnicode;   //BE
        }
        if (b2 == 0x00 && (datasize % 2 == 0))
        {
            for (int i = 0; i < datasize; i = i + 2)
            {
                if (data[i] < 0x06 || data[i] >= 0x7f || data[i + 1] != 0x00)
                {   //半角OnlyのUTF16でもなさそうなのでバイナリ
                    return null;
                }
            }
            return Encoding.Unicode;    //LE;
        }

        //全バイト内容を走査・まずAscii,JIS判定
        int pos = 0;
        int jisCount = 0;
        while (pos < datasize)
        {
            b1 = data[pos];
            if (b1 < 0x03 || b1 >= 0x7f)
            {   //非ascii(UTF,SJis等)発見：次のループへ
                break;
            }
            else if (b1 == 0x1b)
            {   //ESC(JIS)判定
                //2バイト目以降の値を把握
                b2 = ((pos + 1 < datasize) ? data[pos + 1] : (byte)0);
                b3 = ((pos + 2 < datasize) ? data[pos + 2] : (byte)0);
                b4 = ((pos + 3 < datasize) ? data[pos + 3] : (byte)0);
                //B2の値をもとに判定
                if (b2 == 0x24)
                {   //ESC$
                    if (b3 == 0x40 || b3 == 0x42)
                    {   //ESC $@,$B : JISエスケープ
                        jisCount++;
                        pos = pos + 2;
                    }
                    else if (b3 == 0x28 && (b4 == 0x44 || b4 == 0x4F || b4 == 0x51 || b4 == 0x50))
                    {   //ESC$(D, ESC$(O, ESC$(Q, ESC$(P : JISエスケープ
                        jisCount++;
                        pos = pos + 3;
                    }
                }
                else if (b2 == 0x26)
                {   //ESC& : JISエスケープ
                    if (b3 == 0x40)
                    {   //ESC &@ : JISエスケープ
                        jisCount++;
                        pos = pos + 2;
                    }
                }
                else if (b2 == 0x28)
                {   //ESC((28)
                    if (b3 == 0x4A || b3 == 0x49 || b3 == 0x42)
                    {   //ESC(J, ESC(I, ESC(B : JISエスケープ
                        jisCount++;
                        pos = pos + 2;
                    }
                }
            }
            pos++;
        }
        //Asciiのみならここで文字コード決定
        if (pos == datasize)
        {
            if (jisCount > 0)
            {   //JIS出現
                return Encoding.GetEncoding("Shift_JIS");
            }
            else
            {   //JIS未出現。Ascii
                return Encoding.GetEncoding("ASCII");
            }
        }

        bool prevIsKanji = false; //文字コード判定強化、同種文字のときにポイント加算-HNXgrep
        int notAsciiPos = pos;
        int utfCount = 0;
        //UTF妥当性チェック（バイナリ判定を行いながら実施）
        while (pos < datasize)
        {
            b1 = data[pos];
            pos++;

            if (b1 < 0x03 || b1 == 0x7f || b1 == 0xff)
            {   //バイナリ文字：直接脱出
                return null;
            }
            if (b1 < 0x80 || utfCount < 0)
            {   //半角文字・非UTF確定時は、後続処理は行わない
                continue; // 半角文字は特にチェックしない
            }

            //2バイト目を把握、コードチェック
            b2 = ((pos < datasize) ? data[pos] : (byte)0x00);
            if (b1 < 0xC2 || b1 >= 0xf5)
            {   //１バイト目がC0,C1,F5以降、または２バイト目にしか現れないはずのコードが出現、NG
                utfCount = -1;
            }
            else if (b1 < 0xe0)
            {   //2バイト文字：コードチェック
                if (b2 >= 0x80 && b2 <= 0xbf)
                {   //２バイト目に現れるべきコードが出現、OK（半角文字として扱う）
                    if (prevIsKanji == false) { utfCount += 2; } else { utfCount += 1; prevIsKanji = false; }
                    pos++;
                }
                else
                {   //２バイト目に現れるべきコードが未出現、NG
                    utfCount = -1;
                }
            }
            else if (b1 < 0xf0)
            {   //3バイト文字：３バイト目を把握
                b3 = ((pos + 1 < datasize) ? data[pos + 1] : (byte)0x00);
                if (b2 >= 0x80 && b2 <= 0xbf && b3 >= 0x80 && b3 <= 0xbf)
                {   //２/３バイト目に現れるべきコードが出現、OK（全角文字扱い）
                    if (prevIsKanji == true) { utfCount += 4; } else { utfCount += 3; prevIsKanji = true; }
                    pos += 2;
                }
                else
                {   //２/３バイト目に現れるべきコードが未出現、NG
                    utfCount = -1;
                }
            }
            else
            {   //４バイト文字：３，４バイト目を把握
                b3 = ((pos + 1 < datasize) ? data[pos + 1] : (byte)0x00);
                b4 = ((pos + 2 < datasize) ? data[pos + 2] : (byte)0x00);
                if (b2 >= 0x80 && b2 <= 0xbf && b3 >= 0x80 && b3 <= 0xbf && b4 >= 0x80 && b4 <= 0xbf)
                {   //２/３/４バイト目に現れるべきコードが出現、OK（全角文字扱い）
                    if (prevIsKanji == true) { utfCount += 6; } else { utfCount += 4; prevIsKanji = true; }
                    pos += 3;
                }
                else
                {   //２/３/４バイト目に現れるべきコードが未出現、NG
                    utfCount = -1;
                }
            }
        }

        //SJIS妥当性チェック
        pos = notAsciiPos;
        int sjisCount = 0;
        while (sjisCount >= 0 && pos < datasize)
        {
            b1 = data[pos];
            pos++;
            if (b1 < 0x80) { continue; }// 半角文字は特にチェックしない
            else if (b1 == 0x80 || b1 == 0xA0 || b1 >= 0xFD)
            {   //SJISコード外、可能性を破棄
                sjisCount = -1;
            }
            else if ((b1 > 0x80 && b1 < 0xA0) || b1 > 0xDF)
            {   //全角文字チェックのため、2バイト目の値を把握
                b2 = ((pos < datasize) ? data[pos] : (byte)0x00);
                //全角文字範囲外じゃないかチェック
                if (b2 < 0x40 || b2 == 0x7f || b2 > 0xFC)
                {   //可能性を除外
                    sjisCount = -1;
                }
                else
                {   //全角文字数を加算,ポジションを進めておく
                    if (prevIsKanji == true) { sjisCount += 2; } else { sjisCount += 1; prevIsKanji = true; }
                    pos++;
                }
            }
            else if (prevIsKanji == false)
            {
                //半角文字数の加算（半角カナの連続はボーナス点を高めに）
                sjisCount += 1;
            }
            else
            {
                prevIsKanji = false;
            }
        }
        //EUC妥当性チェック
        pos = notAsciiPos;
        int eucCount = 0;
        while (eucCount >= 0 && pos < datasize)
        {
            b1 = data[pos];
            pos++;
            if (b1 < 0x80) { continue; } // 半角文字は特にチェックしない
            //2バイト目を把握、コードチェック
            b2 = ((pos < datasize) ? data[pos] : (byte)0);
            if (b1 == 0x8e)
            {   //1バイト目＝かな文字指定。2バイトの半角カナ文字チェック
                if (b2 < 0xA1 || b2 > 0xdf)
                {   //可能性破棄
                    eucCount = -1;
                }
                else
                {   //検出OK,EUC文字数を加算（半角文字）
                    if (prevIsKanji == false) { eucCount += 2; } else { eucCount += 1; prevIsKanji = false; }
                    pos++;
                }
            }
            else if (b1 == 0x8f)
            {   //１バイト目の値＝３バイト文字を指定
                if (b2 < 0xa1 || (pos + 1 < datasize && data[pos + 1] < 0xa1))
                {   //２バイト目・３バイト目で可能性破棄
                    eucCount = -1;
                }
                else
                {   //検出OK,EUC文字数を加算（全角文字）
                    if (prevIsKanji == true) { eucCount += 3; } else { eucCount += 1; prevIsKanji = true; }
                    pos += 2;
                }
            }
            else if (b1 < 0xa1 || b2 < 0xa1)
            {   //２バイト文字のはずだったがどちらかのバイトがNG
                eucCount = -1;
            }
            else
            {   //２バイト文字OK（全角）
                if (prevIsKanji == true) { eucCount += 2; } else { eucCount += 1; prevIsKanji = true; }
                pos++;
            }
        }

        //文字コード決定
        if (eucCount > sjisCount && eucCount > utfCount)
        {
            return Encoding.GetEncoding("euc-jp");
        }
        else if (utfCount > sjisCount)
        {
            return Encoding.UTF8;
        }
        else if (sjisCount > -1)
        {
            return Encoding.GetEncoding("Shift_JIS");
        }
        else
        {
            return null;
        }
    }
}

public class SelectMenu : MonoBehaviour
{

    AudioClip clip;

    public int position = 0;
    public int samplerate = 44100;
    public float frequency = 440;
    byte[] data;
    int samples;
    int channels;
    string filePath;
    string wavfilepath;
    WWW file;
    float count = 0.0f;
    BufferedWaveProvider provider;

    public bool cantStart;
    bool text_initialize;
    public bool isLoad;
    bool restart;
    int gameLevel;

    List<GameObject> text;
    Text songtext;


    void Start()
    {
        cantStart = true;
        text = new List<GameObject>();
        foreach (Transform T in transform)
        {

            text.Add(T.gameObject);



        }
        //OpenFile();
        //samples = data[2] & 00001100 >> 2;
        //channels = (data[3] & 11000000) >> 6;
        //if (channels == 3)
        //    channels = 1;
        //else
        //{
        //    channels = 2;
        //}

        //clip = AudioClip.Create("test",,channels,samples)

        //AudioClip myClip = AudioClip.Create("Music", samplerate * 2, 1, samplerate, true, OnAudioRead, OnAudioSetPosition);
        //AudioSource aud = GetComponent<AudioSource>();
        //aud.clip = myClip;
        //aud.Play();
    }
    void Update()
    {

        if (FadeScript.isFade)
            return;
        if (cantStart)
            return;
        if (!text_initialize)
        {
            foreach (GameObject gameobj in text)
            {
                gameobj.SetActive(true);
            }
            text_initialize = true;
        }

        count += Time.deltaTime;
        if (count > 1.0f && !isLoad && !cantStart && !restart)
        {
            OpenFile();
            restart = true;


        }
        if (Inputs.Shot && !isLoad && restart)
        {
            restart = false;
        }

        //if (Input.GetButton("Jump") && !isLoad)
        //{
        //    cantStart = true;
        //    isLoad = true;
        //    GameObject.Find("GameSystem").GetComponent<FadeScript>().StartFade(2, "");
        //}
    }


    void OpenFile()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        filePath = OFDDLL.FileDialog.Open(System.IO.Directory.GetCurrentDirectory());
        if (filePath != null)
        {
            isLoad = true;

            SetMusic();
        }
        else
        {
            GameObject.Find("GameSystem").GetComponent<AudioSource>().Stop();
            restart = true;
            return;
        }

    }


    void SetMusic()
    {
        //一般的な44.1kHz, 16bit, ステレオサウンドの音源を想定
        provider = new BufferedWaveProvider(new WaveFormat(44100, 16, 2));

        wavfilepath = System.IO.Directory.GetCurrentDirectory() + "/temp.wav";
        using (var mp3reader = new Mp3FileReader(filePath))
        using (var pcmStream = WaveFormatConversionStream.CreatePcmStream(mp3reader))
        {
            WaveFileWriter.CreateWaveFile(wavfilepath, pcmStream);
        }

        //data = File.ReadAllBytes(wavfilepath);

        ////若干効率が悪いがヘッダのバイト数を確実に割り出して削る
        //using (var r = new WaveFileReader(wavfilepath))
        //{
        //    int headerLength = (int)(data.Length - r.Length);
        //    data = data.Skip(headerLength).ToArray();
        //}

        //int bufsize = 16000;
        //for (int i = 0; i + bufsize < data.Length; i += bufsize)
        //{
        //    provider.AddSamples(data, i, bufsize);
        //}

        string title = "";
        string artist = "";


        Mp3FileReader reader = new Mp3FileReader(filePath);
        char[] trimChars = { '\0' };
        bool bad_tag = false;
        Encoding encoding = Encoding.Unicode;
        bool readId3v2 = reader.Id3v2Tag != null;
        bool readId3v1 = reader.Id3v2Tag == null;
        if (readId3v2)
        {
            byte[] v2 = reader.Id3v2Tag.RawData;

            // タグはID3v2.3であると仮定し、ヘッダを無視

            Dictionary<string, string> tags = new Dictionary<string, string>();
            for (int index = 10; index < v2.Length; )
            {
                if (v2[index] == '\0') break;

                string frameID = Encoding.ASCII.GetString(v2, index, 4);
                index += 4;

                if (BitConverter.IsLittleEndian)
                {
                    Array.Reverse(v2, index, 4);
                }
                int frameSize = BitConverter.ToInt32(v2, index);
                index += 4;

                byte flag1 = v2[index++];
                byte flag2 = v2[index++];

                bool isBOM = false; ;

                bool isIDv24;



                if (frameID[0] == 'T') // テキスト情報フレームならば読み込む
                {

                    byte[] temp;



                    switch (v2[index++])
                    {
                        case 0x00:
                            if (!bad_tag)
                            {
                                try
                                {
                                    temp = new byte[frameSize - 1];
                                    Array.Copy(v2, index + 1, temp, 0, frameSize - 1);

                                    encoding = CharCodeDetector.detectCharCode(temp, temp.Length);
                                }
                                catch
                                {
                                    encoding = null;
                                    bad_tag = false;
                                }
                                if (encoding != null)
                                {
                                    Debug.Log(encoding.GetType());
                                    if (encoding != Encoding.ASCII)
                                        bad_tag = true;
                                }
                            }
                            break;

                        case 0x01:

                            if (index + 1 < v2.Length && (v2[index] == 0xfe && v2[index + 1] == 0xff))
                            {
                                encoding = Encoding.BigEndianUnicode; // UTF-16BE
                                isBOM = true;

                            }
                            else if (index + 1 < v2.Length && (v2[index] == 0xff && v2[index + 1] == 0xfe))
                            {

                                encoding = Encoding.Unicode;
                                isBOM = true;
                            }
                            break;
                        case 0x02:
                            {

                                encoding = Encoding.BigEndianUnicode; // UTF-16BE
                            }
                            break;
                        case 0x03:
                            {
                                if (!bad_tag)
                                {
                                    try
                                    {
                                        temp = new byte[frameSize - 1];
                                        Array.Copy(v2, index + 1, temp, 0, frameSize - 1);

                                        encoding = CharCodeDetector.detectCharCode(temp, temp.Length);
                                    }
                                    catch {
                                        encoding = null;
                                        bad_tag = false;
                                    };
                                    if (encoding != null)
                                    {
                                        Debug.Log(encoding.GetType());
                                        if (encoding != Encoding.ASCII)
                                            bad_tag = true;
                                    }
                                }
                            }
                            break;
                        default:
                            break;

                    }

                    if (index + 1 < v2.Length && (v2[index] == 0xfe && v2[index + 1] == 0xff) || index + 1 < v2.Length && (v2[index] == 0xff && v2[index + 1] == 0xfe))
                    {
                        isBOM = true;

                    }

                    //}
                    string content = "";

                    if (encoding != null)
                    {

                        try
                        {
                            if (!isBOM)
                                content = encoding.GetString(v2, index, frameSize - 1);
                            else
                                content = encoding.GetString(v2, index, frameSize - 3);
                        }
                        catch
                        {
                            Debug.Log("a");
                            content = "";
                        };
                    }
                    index += frameSize - 1;

                    tags.Add(frameID, content);

                }

                else
                {
                    index += frameSize;
                }
            }

            tags.TryGetValue("TIT2", out title);
            tags.TryGetValue("TPE1", out artist);
            if (title != null)
                title.TrimEnd(trimChars);
            if (artist != null)
                artist.TrimEnd(trimChars);

            //string album = tags["TALB"];
        }
        if (readId3v1)
        {

            byte[] v1 = reader.Id3v1Tag;

            //Encoding encoding = Encoding.UTF8;

            byte[] temp;


            try
            {
                temp = new byte[30];
                Array.Copy(v1, 3, temp, 0, 30);

                encoding = CharCodeDetector.detectCharCode(temp, temp.Length);




                title = encoding.GetString(v1, 3, 30);
            }
            catch { title = ""; };
            if (title != null)
                title.TrimEnd(trimChars);
            try
            {
                temp = new byte[30];
                Array.Copy(v1, 33, temp, 0, 30);

                encoding = CharCodeDetector.detectCharCode(temp, temp.Length);

                artist = encoding.GetString(v1, 33, 30);
            }
            catch { artist = ""; };
            if (artist != null)
                artist.TrimEnd(trimChars);

        }

        MusicController.FileProperty property = new MusicController.FileProperty();

        property.artist = artist;
        property.title = title;

        MusicController.Property = property;

        //else
        //{
        //    Mp3FileReader reader = new Mp3FileReader(fileName);
        //    byte[] v2 = reader.Id3v2Tag.RawData;

        //    // タグはID3v2.3であると仮定し、ヘッダを無視

        //    Dictionary<string, string> tags = new Dictionary<string, string>();
        //    for (int index = 10; index < v2.Length; )
        //    {
        //        if (v2[index] == '\0') break;

        //        string frameID = Encoding.ASCII.GetString(v2, index, 4);
        //        index += 4;

        //        if (BitConverter.IsLittleEndian)
        //        {
        //            Array.Reverse(v2, index, 4);
        //        }
        //        int frameSize = BitConverter.ToInt32(v2, index);
        //        index += 4;

        //        byte flag1 = v2[index++];
        //        byte flag2 = v2[index++];

        //        if (frameID[0] == 'T') // テキスト情報フレームならば読み込む
        //        {
        //            Encoding encoding = Encoding.Unicode; // UTF-16LE
        //            switch (v2[index++])
        //            {
        //                case 0x00:
        //                    encoding = Encoding.GetEncoding(28591); // iso-8859-1
        //                    break;

        //                case 0x01:
        //                    if (index + 1 < v2.Length && (v2[index] == 0xfe && v2[index + 1] == 0xff))
        //                    {
        //                        encoding = Encoding.BigEndianUnicode; // UTF-16BE
        //                    }
        //                    break;
        //            }

        //            string content = encoding.GetString(v2, index, frameSize - 1);
        //            index += frameSize - 1;

        //            tags.Add(frameID, content);
        //        }
        //        else
        //        {
        //            index += frameSize;
        //        }
        //    }


        //    string title = tags["TIT2"];
        //    string artist = tags["TPE1"];
        //    string album = tags["TALB"];
        //}
        file = new WWW("file:///" + wavfilepath);

        StartCoroutine(CreateAudioClip());

        return;



        // AudioClip.Create("play",)

        //file = new WWW("file:///"+filePath);
        //if (!System.IO.File.Exists(filePath))
        //{
        //    Debug.Log("File does NOT exist!! file path = " + file);

        //}
        //while (file.isDone)
        //{
        //    yield return 0;
        //}
        //clip = file.GetAudioClip(false,false,AudioType.MPEG);
        //while(clip.loadState == AudioDataLoadState.Loading)
        //{
        //    yield return new WaitForEndOfFrame();
        //}
        //if (clip.loadState == AudioDataLoadState.Failed)
        //    yield return 0;//失敗
    }

    IEnumerator CreateAudioClip()
    {
        while (!file.isDone)
        {
            yield return new WaitForFixedUpdate();
        }
        clip = file.GetAudioClip(false, false, AudioType.WAV);
        while (clip.loadState == AudioDataLoadState.Loading)
        {
            yield return new WaitForFixedUpdate();
        }
        if (clip.loadState == AudioDataLoadState.Failed)
        {
            yield return 0;
        }
        //Window.SetActiveWindow();
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        MusicController.instance.setAudioClip(clip);

    }

    void OnAudioRead(float[] data)
    {
        int count = 0;
        while (count < data.Length)
        {
            ;
        }
    }
    void OnAudioSetPosition(int newPosition)
    {
        position = newPosition;
    }

    void OnDestroy()
    {
        if (System.IO.File.Exists(wavfilepath))
            System.IO.File.Delete(wavfilepath);
    }


}
