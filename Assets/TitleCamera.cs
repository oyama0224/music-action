﻿using UnityEngine;
using System.Collections;

public class TitleCamera : MonoBehaviour {
	Vector3 basePos = new Vector3(0,10,0);
	Vector3 stageSize;
	Vector3 Position;
	Transform stage;
	float count;
	const float rotationTime = 5.0f;
	// Use this for initialization
	void Start () {
		stage = GameObject.Find ("TitleStage").transform;
		stageSize = stage.transform.localScale; 
	}
	
	// Update is called once per frame
	void Update () {
		count += Time.deltaTime;
		if (count < rotationTime) {
			count -= rotationTime;
		}
		Position = basePos + new Vector3 (Mathf.Sin (count / rotationTime * Mathf.PI * 2) * stageSize.x, 0, Mathf.Cos (count / rotationTime * Mathf.PI * 2) * stageSize.z);
		transform.position = Position;
		transform.LookAt (basePos);
	}
}
