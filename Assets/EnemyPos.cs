﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class EnemyPos : MonoBehaviour
{
    EnemyManager enemymanager;
    public List<Transform> showList;
    public List<GameObject> showUI;
    Transform player;
    string iconPath = "UI/Icon/";

    public Sprite Slime;
    Sprite Torol;
    Sprite Kangaru;
    Sprite Tori;
    public Sprite[] Enemy;

    public bool xover, yover, zover;

    Vector2 offset;

    const float distance = 60.0f;
    
    GameObject basePrefab;
    Vector2 baseSize;
    // Use this for initialization
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        enemymanager = GameObject.Find("EnemyManager").GetComponent<EnemyManager>();
        showList = new List<Transform>();
        showUI = new List<GameObject>();
        Slime = Resources.Load<Sprite>(iconPath + "slime_a");
        Torol = Resources.Load<Sprite>(iconPath + "torol_a");
        Kangaru = Resources.Load<Sprite>(iconPath + "kangaru_a");
        Tori = Resources.Load<Sprite>(iconPath + "tori_a");
        Enemy = new Sprite[4] { Slime, Torol, Kangaru, Tori };
        basePrefab = Resources.Load<GameObject>("EnemyUI_base");
        baseSize = new Vector2(51.2f, 37.7f);

        baseSize.x *= Screen.height / 480.0f;
        baseSize.y *= Screen.height / 480.0f;

        basePrefab.GetComponent<RectTransform>().sizeDelta = baseSize;

        offset = basePrefab.GetComponent<Image>().rectTransform.sizeDelta;
    }
    void OnGUI()
    {
        for (int i = 0; i < showUI.Count; i++)
        {
            if (showList[i] == null)
                continue;

            Vector3 temp = Camera.main.transform.InverseTransformPoint(showList[i].position) - Camera.main.transform.InverseTransformPoint(Camera.main.transform.position);

            float rad;
            if (temp.z >= temp.y)
                rad = Mathf.Atan2(temp.z, temp.x);
            else
            {
                rad = Mathf.Atan2(temp.y, temp.x);
            }

            float sin,cos;
            sin = Mathf.Sin(rad);
            cos = Mathf.Cos(rad);
            if (Mathf.Abs(sin) > Mathf.Abs(cos))
            {
                sin = Mathf.Sign(sin);
            }
            else
            {
                cos = Mathf.Sign(cos);
            }

            RectTransform rectT = showUI[i].GetComponent<RectTransform>();
            Vector3 position = new Vector3(Screen.width / 2 + Screen.width / 2 * cos + (baseSize.x / 2 * Mathf.Sign(-cos)), Screen.height / 2 + Screen.height / 2 * sin + (baseSize.y / 2 * Mathf.Sign(-sin)));
            rectT.transform.position = position;
            rectT.sizeDelta = baseSize * ((distance +30  - Vector3.Distance(Camera.main.transform.position,showList[i].position)) / distance);

            Vector3 viewtemp = Camera.main.WorldToViewportPoint(showList[i].position);
            bool outCamera = Mathf.Abs(viewtemp.x) > 1 || Mathf.Abs(viewtemp.y) > 1 || viewtemp.z < 0;
            if (outCamera)
            {
                showUI[i].SetActive(true);
               
            }
            else
            {
                showUI[i].SetActive(false);
            }
            //Vector3 uipos = Camera.main.WorldToScreenPoint(showList[i].position);

            //xover = uipos.x < 0 || uipos.x > Screen.width;
            //yover = uipos.y < 0 || uipos.y > Screen.height;
            //zover = uipos.z < 0;
            //if (xover || yover)
            //{

            //    showUI[i].SetActive(true);

            //    if (zover)
            //    {
            //        uipos.x = Mathf.Clamp(Screen.width - uipos.x, 0 + offset.x / 2, Screen.width - offset.x / 2);
            //        uipos.y = Mathf.Clamp(Screen.height - uipos.y, 0 + offset.y / 2, Screen.height - offset.y / 2);
            //    }
            //    else
            //    {
            //        uipos.x = Mathf.Clamp(uipos.x, 0 + offset.x / 2, Screen.width - offset.x / 2);
            //        uipos.y = Mathf.Clamp(uipos.y, 0 + offset.y / 2, Screen.height - offset.y / 2);                    
            //    }

                
            //    showUI[i].GetComponent<RectTransform>().position = uipos;
            //}
            //else
            //{
            //    if (zover)
            //    {
            //        showUI[i].SetActive(true);
            //        uipos.x = Mathf.Clamp(Screen.width - uipos.x, 0 + offset.x / 2, Screen.width - offset.x / 2);
            //        uipos.y = Mathf.Clamp(Mathf.Sign(uipos.y - Screen.width / 2) * Screen.height, 0 + offset.y / 2, Screen.height - offset.y / 2);
            //        showUI[i].GetComponent<RectTransform>().position = uipos;
            //    }
            //    else
            //    {
            //        showUI[i].SetActive(false);
            //    }
            //}



        }
    }


    // Update is called once per frame
    void Update()
    {
        foreach (Transform T in EnemyManager.enemyList)
        {
            if(T == null)
                continue;
            if (Vector3.Distance(Camera.main.transform.position, T.position) <= distance)
            {
                if (showList.IndexOf(T) == -1)
                {
                    showList.Add(T);
                    GameObject UI = GameObject.Instantiate<GameObject>(basePrefab);
                    UI.GetComponent<Image>().sprite = Enemy[T.GetComponent<EnemyTestScript>().type];
                    UI.transform.SetParent(transform);
                    UI.SetActive(false);
                    showUI.Add(UI);
                    
                }
            }
        }

        for (int i = 0; i < showList.Count; i++)
        {
            if (showList[i] == null)
            {
                showList.Remove(showList[i]);
                GameObject temp = showUI[i];
                showUI.Remove(showUI[i]);
                Destroy(temp);
                continue;
            }
            if (Vector3.Distance(Camera.main.transform.position, showList[i].position) > distance)
            {
                showList.Remove(showList[i]);
                GameObject temp = showUI[i];
                showUI.Remove(showUI[i]);
                Destroy(temp);
            }
        }
    }
}
