﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class LevelSelect : MonoBehaviour
{

    string clipPath = "BGM/";
    AudioClip ok;
    AudioClip up;
    AudioClip down;

    AudioSource source;

    public int selectLevel;
    float count;
    public List<GameObject> menu;

    SelectMenu selectmenu;

    string menuPath = "UI/Menu/";

    Sprite[] SelectImage;
    Sprite[] UnSelectImage;

    Sprite[] Select;
    Sprite[] Onpu;
    Sprite[] End;

    Vector3[] basePos;

    Color SelectColor;
    Color UnSelectColor;

    int SelectSize;
    int UnSelectSize;

    Vector2 SelectSpriteSize;
    Vector2 UnSelectSpriteSize;

    const int StartMenuCount = 2;   //ゲーム開始するメニューの数
    const int DestroyMenu = 2;      //終了メニューの位置

    bool inName;
    bool inNameActive;
    GameObject inNameObject;
    GameObject nameTitle;
    GameObject IME;
    Vector2 ScreenRate;

    string userName;

    bool IME_ON;





    void OnGUI()
    {

        if (inName)
        {
            nameTitle.GetComponent<Text>().color = SelectColor;
            nameTitle.GetComponent<Text>().fontSize = SelectSize;
        }
        else
        {
            nameTitle.GetComponent<Text>().color = UnSelectColor;
            nameTitle.GetComponent<Text>().fontSize = UnSelectSize;
        }
        for (int i = 0; i < menu.Count; i++)
        {
            if (i == selectLevel && !inName)
            {
                if (i != menu.Count - 1)
                {
                    menu[i].GetComponent<Image>().sprite = SelectImage[i];
                    menu[i].GetComponent<RectTransform>().position = new Vector3(basePos[i].x, basePos[i].y);
                    menu[i].GetComponent<RectTransform>().sizeDelta = new Vector2(SelectSpriteSize.x, SelectSpriteSize.y);
                }
                menu[i].transform.GetChild(0).GetComponent<Text>().color = SelectColor;
                menu[i].transform.GetChild(0).GetComponent<Text>().fontSize = SelectSize;
            }
            else
            {
                if (i != menu.Count - 1)
                {
                    menu[i].GetComponent<Image>().sprite = UnSelectImage[i];
                    menu[i].GetComponent<RectTransform>().position = new Vector3(basePos[i].x, basePos[i].y);
                    menu[i].GetComponent<RectTransform>().sizeDelta = new Vector2(UnSelectSpriteSize.x, UnSelectSpriteSize.y);
                }
                menu[i].transform.GetChild(0).GetComponent<Text>().color = UnSelectColor;
                menu[i].transform.GetChild(0).GetComponent<Text>().fontSize = UnSelectSize;
            }

        }
    }

    // Use this for initialization
    void Start()
    {
        selectmenu = GameObject.Find("SelectSystem").GetComponent<SelectMenu>();
        selectmenu.cantStart = true;

        Select = new Sprite[2];
        Onpu = new Sprite[2];
        End = new Sprite[2];

        ok = Resources.Load<AudioClip>(clipPath + "menu_ok");
        up = Resources.Load<AudioClip>(clipPath + "menu_up");
        down = Resources.Load<AudioClip>(clipPath + "menu_down");

        source = GetComponent<AudioSource>();

        for (int i = 0; i < 2; i++)
        {
            Select[i] = Resources.Load<Sprite>(menuPath + "開始" + i);
            Onpu[i] = Resources.Load<Sprite>(menuPath + "音符" + i);
            End[i] = Resources.Load<Sprite>(menuPath + "終了" + i);
        }
        menu = new List<GameObject>();
        foreach (Transform T in transform.FindChild("Menu"))
        {

            menu.Add(T.gameObject);

        }

        inNameObject = GameObject.Find("InputField");
        nameTitle = GameObject.Find("NameTitle");

        IME = GameObject.Find("IME");

        userName = ScoreManager.GetUserName();
        inNameObject.GetComponent<InputField>().text = userName;

        SelectSpriteSize = menu[0].GetComponent<RectTransform>().sizeDelta;
        UnSelectSpriteSize = menu[1].GetComponent<RectTransform>().sizeDelta;

        basePos = new Vector3[3];
        SelectImage = new Sprite[3];
        UnSelectImage = new Sprite[3];

        ScreenRate.x = Screen.width / 640;
        ScreenRate.y = Screen.height / 480;

        for (int i = 0; i < SelectImage.Length; i++)
        {
            basePos[i] = menu[i].GetComponent<RectTransform>().position;
            if (i < StartMenuCount)
            {
                SelectImage[i] = Select[1];
                UnSelectImage[i] = Onpu[0];
            }
            else
            {
                if (i == DestroyMenu)
                {
                    SelectImage[i] = End[1];
                    UnSelectImage[i] = End[0];

                }
            }
        }

        //SelectImage = menu[0].GetComponent<Image>().sprite;
        //UnSelectImage = menu[1].GetComponent<Image>().sprite;

        SelectColor = menu[0].transform.GetChild(0).GetComponent<Text>().color;
        UnSelectColor = menu[1].transform.GetChild(0).GetComponent<Text>().color;

        SelectSize = menu[0].transform.GetChild(0).GetComponent<Text>().fontSize;
        UnSelectSize = menu[1].transform.GetChild(0).GetComponent<Text>().fontSize;
    }


    // Update is called once per frame
    void Update()
    {
        //IME.SetActive();
        if (!inName)
        {
            if (Inputs.move.z < 0)
            {
                if (selectLevel < menu.Count - 1 && count == 0.0f)
                {
                    selectLevel++;
                    source.PlayOneShot(down);
                }
                count += Time.deltaTime;
            }
            else if (Inputs.move.z > 0)
            {
                if (selectLevel > 0 && count == 0.0f)
                {
                    selectLevel--;
                    source.PlayOneShot(up);
                }

                count += Time.deltaTime;
            }
            else
            {
                if (Inputs.move.x > 0.5f)
                {
                    inName = true;
                    source.PlayOneShot(down);
                }
                count = 0.0f;
            }
            MusicController.GameLevel = selectLevel;

            if (Inputs.Jump)
            {
                source.PlayOneShot(ok);
                if (selectLevel == menu.Count - 1)
                {
                    ScoreManager.SetUserName(userName);
                    GameObject.Find("GameSystem").GetComponent<FadeScript>().StartFade(3, "");
                    Destroy(inNameObject);
                    Destroy(gameObject);
                }
                else if (selectLevel == 2)
                {
                    Application.Quit();
                }
                else
                {
                    ScoreManager.SetUserName(userName);
                    selectmenu.cantStart = false;
                    Destroy(gameObject);
                    Destroy(inNameObject);
                }
            }
            
        }
        else
        {
            if (Inputs.move.x < -0.5f && !inNameActive)
            {
                inName = false;
                
                source.PlayOneShot(up);
            }
            if (Inputs.Jump && !inNameActive)
            {
                inNameActive = true;
                Input.imeCompositionMode = IMECompositionMode.Off;
                inNameObject.GetComponent<InputField>().ActivateInputField();
                
            }
            //if(inNameActive && (Input.GetKey(KeyCode.Return) || Input.GetKey(KeyCode.Escape))){
            //    inNameActive = false;
            //}
        }

        
    }

    public void OnEndEdit()
    {
        userName = inNameObject.GetComponent<InputField>().text;
        inNameActive = false;
        inName = false;
        Input.ResetInputAxes();
    }
}
