﻿using UnityEngine;
using System.Collections;

public class Target : MonoBehaviour {
    
	// Use this for initialization
	void Start () {
        RectTransform rt;
        rt = GetComponent<RectTransform>();
        Vector3 position = rt.position;
        //position.y *= Screen.height / 480.0f;


        Vector2 size = rt.sizeDelta;

        size.x *= Screen.height / 480.0f;
        size.y *= Screen.height / 480.0f;

        rt.sizeDelta = size;
        rt.position = position;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
