﻿using UnityEngine;
using System.Collections;

public class PlayerControll : MonoBehaviour
{

    public enum State
    {
        Stay,
        Walk,
        Jump,
        Atack,
        Guard,
        Damage
    };

   AudioClip[] AtackSE;
   AudioClip[] DamageSE;

   AudioSource source;

    public State state;
    public int animation_state;
    public Vector3 Move;
    Vector3 beforeMove;

    float m_JumpAcceleration;

    float jumpCount;
    float baseAngle;
    public float sideRotation = 0.0f;
    bool isJump;

    bool m_anglef;
    float m_addangleY;
    float m_anglerate = 5.0f;

    bool isRotation;    //反転中
    public bool isDash;

    const float playersize = 1.7f;

    Animator animator;
    bool isAnimationChange;
    AnimatorStateInfo oldState;

    bool nextAtack;

    bool isAtackSE;

    WeaponControll weaponCtrl;

    int damageCount;

    bool GuardBlock;
    float mutekiTime;

    public Vector3 baseangle;

    float damageTime;

    CameraScript cameraScr;

    public bool isReturn;

    float ReturnCount = 0;

    public bool isMove{
        
        get{
            return Move.magnitude > 0.00f;
        }   
        
    }

    public bool isDamage
    {
        get
        {
            return state == State.Damage;
        }
    }

    Vector3 beforePos;

    // Use this for initialization
    void Start()
    {
        animator = GetComponent<Animator>();
        weaponCtrl = GetComponent<WeaponControll>();
        oldState = animator.GetCurrentAnimatorStateInfo(0);
        cameraScr = Camera.main.GetComponent<CameraScript>();
        source = GetComponent<AudioSource>();

        AtackSE = new AudioClip[2];
        DamageSE = new AudioClip[2];


        AtackSE[0] = Resources.Load<AudioClip>("BGM/Player/atack0");
        AtackSE[1] = Resources.Load<AudioClip>("BGM/Player/atack1");
        DamageSE[0] = Resources.Load<AudioClip>("BGM/Player/damage0");
        DamageSE[1] = Resources.Load<AudioClip>("BGM/Player/damage1");
        //AnimationChange("Next");

        GameObject onpuEffect = Resources.Load<GameObject>("OnpuEffect");

    }

    // Update is called once per frame
    void Update()
    {


        beforeMove = Move;

        animation_state = animator.GetInteger("State");

        isDash = Inputs.Dash;

        bool isCantMove = (state == State.Atack || state == State.Damage);

        if ((Inputs.move.z != 0.0f && !isCantMove)||(Inputs.move.x != 0 && !isCantMove))
        {
            //パッド用？
            //if(Inputs.move.z < 0.0f)
            //{
            //    transform.rotation = Quaternion.AngleAxis(180 , new Vector3(0, 1, 0));
            //}

            Move = Vector3.ClampMagnitude(Move + Inputs.move * Time.deltaTime, Inputs.move.magnitude * 0.75f);

            //Move.z = Mathf.Clamp(Move.z + (Inputs.move.z * Time.deltaTime), -Mathf.Abs(Move.z / Inputs.move.magnitude), Mathf.Abs(Move.z / Inputs.move.magnitude));
            

        }
        else
        {
            Move.z -= (Move.z * Time.deltaTime);
            if (Mathf.Abs(Move.z) < 0.1f)
                Move.z = 0.0f;
            Move.x -= Move.x * Time.deltaTime;
            if (Mathf.Abs(Move.x) < 0.1f)
                Move.x = 0.0f;
        }


        //Debug.Log(Move.magnitude);
        //if (Inputs.move.x != 0 && !isCantMove)
        //{


         
        //        if (!Inputs.useController)
        //        {
        //            //sideRotation = (sideRotation + Inputs.move.x  * 0.1f * Time.deltaTime);
        //            Move.x = Mathf.Clamp(Move.x + (Inputs.move.x * Time.deltaTime), -Mathf.Abs(Move.x / Inputs.move.magnitude), Mathf.Abs(Move.x/ Inputs.move.magnitude));
        //        }
        //        else
        //        {
        //            Move.x = Inputs.move.x * 10 * Time.deltaTime;
        //        }

            


            
        //}
        //else
        //{

        //    Move.x -= Move.x * Time.deltaTime;
        //    if (Mathf.Abs(Move.x) < 0.1f)
        //        Move.x = 0.0f;
        //}

        if ((!isDash && !isCantMove) || state == State.Guard)
        {
            if (Mathf.Abs(Move.magnitude) > Mathf.Abs(Inputs.move.magnitude) * 0.5f)
                Move = Vector3.ClampMagnitude(Move, Inputs.move.magnitude * 0.5f);
        }






        //transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y + sideRotation * 100 * Time.deltaTime, transform.eulerAngles.z);



        //if (m_addangleY != 0)
        //{
        //    m_addangleY = 0;
        //}

        bool isCantShot = state == State.Guard;

        if (Inputs.Shot && !isCantShot)
        {
            weaponCtrl.shot = true;

        }
        else
        {
            weaponCtrl.shot = false;
        }

        bool isCantAtack = (state == State.Atack || state == State.Guard) || state == State.Damage;
        bool isCantGuard = ((state == State.Atack || state == State.Damage) || state == State.Guard);
        float animationTime = animator.GetCurrentAnimatorStateInfo(0).normalizedTime;

        if (Inputs.Atack && !isCantAtack && state != State.Atack)
        {
            state = State.Atack;
            nextAtack = false;
            isAtackSE = false;
            animator.speed = 1.0f;
            animator.Play("Atack", 0, 0.0f);
        }

        if (state == State.Atack && animation_state == (int)state)
        {
            bool endAtack = false;

            if (animationTime <0.3f || animationTime > 0.8f)
                Move.z = 50.0f * Time.deltaTime;

            if (animationTime > 0.25f && !isAtackSE)
            {
                source.PlayOneShot(AtackSE[0]);
                isAtackSE = true;
            }
            weaponCtrl.isAtack = true;
            animator.speed = 1.0f;
            if (!nextAtack && animationTime > 0.7f)
            {
                if (Inputs.Atack)
                {
                    source.PlayOneShot(AtackSE[1]);
                    nextAtack = true;
                }
                else
                {
                    endAtack = true;
                }
            }
            if (animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1.0f || endAtack)
            {
                Move.z = 0.0f;
                state = State.Walk;
                nextAtack = false;
                animator.Play("Stay", 0, 0.0f);
            }
        }
        else
        {
            weaponCtrl.isAtack = false;
        }

        if (Inputs.Guard && !isCantGuard)
        {
            state = State.Guard;
        }
        if (state == State.Guard && animation_state == (int)state)
        {
            weaponCtrl.isGuard = true;
            if (Inputs.Guard && animationTime >= 0.5f && animationTime <= 0.6f)
            {
                GuardBlock = true;
                animator.speed = 0.0f;
                animator.Play("Guard", 0, 0.5f);
            }
            else
            {
                GuardBlock = false;
                animator.speed = 1.0f;
            }
            if (animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1.0f)
            {
                state = State.Stay;
            }
        }
        else
        {
            weaponCtrl.isGuard = false;
            GuardBlock = false;
        }

        if (state == State.Damage && animation_state == (int)state)
        {
            damageTime += Time.deltaTime;
            if (animationTime >= 1.0f && damageTime > 1.0f)
            {
                state = State.Stay;
                damageCount = 0;
                damageTime = 0.0f;
                mutekiTime = 1.5f;
                animator.Play("Stay", 0, 0.0f);
            }
        }
        else
        {
            if (mutekiTime > 0.0f)
                mutekiTime -= Time.deltaTime;

        }

        bool isCantJump = ((state == State.Atack || state == State.Guard) || state == State.Damage);

        if (Inputs.Jump && !isCantJump)
        {
            if (!isJump)
            {
                isJump = true;
                jumpCount = 0.0f;
                m_JumpAcceleration = 0.3f;
                state = State.Jump;
            }
        }

        if (isJump)
        {

            Move.z -= Move.z * 0.01f * Time.deltaTime;
            if (jumpCount < 1.5f)//上昇～最大落下到達前
            {
                if (Inputs.Jump)
                {
                    jumpCount += Time.deltaTime;
                }
                else
                {
                    jumpCount += Time.deltaTime * 2.5f;   //早く落下するように
                }
                Move.y = Mathf.Sin(jumpCount * Mathf.PI) * 0.3f;
            }
            else
            {
                jumpCount = 1.5f;
                Move.y = Mathf.Sin(jumpCount * Mathf.PI) * (m_JumpAcceleration += (Time.deltaTime));
            }
            if (!isCantJump)
            {
                state = State.Jump;
                animator.Play("Jump", 0, jumpCount / 1.5f);
            }

        }

        //着地判定
        Debug.DrawRay(transform.position + new Vector3(0, playersize / 2, 0), Vector3.down, Color.red, playersize / 2);
        RaycastHit hit;

        float distance = 0.0f;
        if (Move.y < 0)
        {
            distance = Mathf.Abs(Move.y);
        }

        if (Physics.Raycast(transform.position + new Vector3(0, playersize, 0), Vector3.down, out hit, playersize  + 0.5f + distance, 1 << 8))
        {
            if (!(isJump && jumpCount < 1.0f))
            {
                transform.position = new Vector3(transform.position.x, hit.point.y, transform.position.z);
                Move.y = 0.0f;
                isJump = false;
                //Debug.Log("jump");
                if (state == State.Jump)
                {
                    state = State.Stay;
                }
            }
            //else
            //{
            //    Debug.Log("jump");
            //}
        }
        else
        {
            if (!isJump)//着地していないとき
            {
                //落下開始
                isJump = true;
                jumpCount = 1.0f;
            }
        }
        Quaternion test;
        if (state != State.Damage)
        {
            test = Camera.main.transform.rotation;
            test.x = 0.0f;
            test.z = 0.0f;
        }
        else
        {
            test = transform.rotation;
            test.x = 0.0f;
            test.z = 0.0f;
        }

        Debug.DrawRay(transform.position + (test * (Move * 50 * Time.deltaTime)) + new Vector3(0, playersize , 0), Vector3.down,Color.green ,Mathf.Infinity);


        bool side = (Physics.Raycast(transform.position + new Vector3(0, playersize / 2, 0), (test * (Move * 50 * Time.deltaTime)), out hit, Move.magnitude, 1 << 8) || (Physics.Raycast(transform.position + new Vector3(0, playersize - playersize / 2, 0), (test * (Move * 50 * Time.deltaTime)), out hit, Move.magnitude, 1 << 8) ));

        if (side)
        {
           Vector3 normal = hit.normal;
            Vector3 temp = hit.point - transform.position;
            temp.y = 0;

            transform.position += normal * Move.magnitude;
            Move = Vector3.zero;
        }
        bool isGroundR = Physics.Raycast(transform.position + (test * (Move * 50 * Time.deltaTime)) + test * new Vector3(0.4f, playersize / 2, 0), Vector3.down, Mathf.Infinity, 1 << 8);
        bool isGroundL = Physics.Raycast(transform.position + (test * (Move * 50 * Time.deltaTime)) + test * new Vector3(-0.4f, playersize / 2, 0), Vector3.down, Mathf.Infinity, 1 << 8);
        bool isGroundF = Physics.Raycast(transform.position + (test * (Move * 50 * Time.deltaTime)) + test * new Vector3(0f, playersize / 2, 0.4f), Vector3.down, Mathf.Infinity, 1 << 8);
        bool isGroundB = Physics.Raycast(transform.position + (test * (Move * 50 * Time.deltaTime)) + test * new Vector3(0f, playersize / 2, -0.4f), Vector3.down, Mathf.Infinity, 1 << 8);
        bool isGround = isGroundR && isGroundL && isGroundF && isGroundB;
        if (!isGround)
        {
            Vector3 temp = Vector3.zero;
            if (isGroundR)
            {
                temp.x = 0.1f;
            }
            if (isGroundL)
            {
                temp.x = -0.1f;
            }
            if(isGroundF)
            {
                temp.z = 0.1f;
            }
            if(isGroundB)
            {
                temp.z = -0.1f;
            }




            //if (Move.y > 0)
            //{
            //    Move.y = 0;
            //    jumpCount = 1.0f * Mathf.PI;
            if (state == State.Jump)
            {
                state = State.Stay;
            }
            //}

            Move = new Vector3(temp.x,Move.y,temp.z);
            if(state != State.Damage){
                state = State.Stay;
            }
            //transform.position = beforePos;
        }

        bool cantMoveState = ((state == State.Jump || state == State.Atack) || (state == State.Damage || state == State.Guard));


        
        
        if (Move != Vector3.zero)
        {
            beforePos = transform.position;
            //transform.eulerAngles = new Vector3(transform.eulerAngles.x, Camera.main.transform.eulerAngles.y + (Mathf.Atan2(Move.z, -Move.x) - Mathf.PI * 0.5f) * Mathf.Rad2Deg, transform.eulerAngles.z);
            Debug.DrawRay(transform.position, test * (Move * 50 * Time.deltaTime),Color.cyan);
            transform.position += test * (Move * 50 * Time.deltaTime);



            if (state != State.Damage)
            {
                if (state == State.Jump && Move.x == 0 && Move.z == 0)
                {
                    //transform.eulerAngles = new Vector3(transform.eulerAngles.x, Camera.main.transform.eulerAngles.y, transform.eulerAngles.z);
                }
                else
                {
                    transform.eulerAngles = new Vector3(transform.eulerAngles.x, test.eulerAngles.y + (Mathf.Atan2(Move.z, -Move.x) - Mathf.PI * 0.5f) * Mathf.Rad2Deg, transform.eulerAngles.z);
                }
            }

            if(ReturnCount < 10.0f)
                ReturnCount += Mathf.Abs(((Move * 50 * Time.deltaTime)).z) + Mathf.Abs(((Move * 50 * Time.deltaTime)).x);
            else
            {
                isReturn = true;
            }

            //Camera.main.transform.position += Camera.main.transform.TransformDirection(Move * 50 * Time.deltaTime);
            if (!cantMoveState && !isJump)
            {
                state = State.Walk;
                animator.speed = Mathf.Abs(Move.magnitude * 4);
            }
        }
        else
        {
            
            if ((cameraScr.MoveCamera_under ||!cameraScr.MoveCamer_side) && !isMove)
                transform.rotation = Quaternion.Lerp(transform.rotation, test, 12.5f * Time.deltaTime);
            if (!cantMoveState && !isJump)
            {
                isReturn = false;
                ReturnCount = 0.0f;
                animator.speed = 1.0f;
                state = State.Stay;
            }
        }


        animator.SetInteger("State", (int)state);

        //if (Mathf.Abs(Move.z) > 0.1f)
        //{

        //    if (animator.GetCurrentAnimatorStateInfo(0).fullPathHash == Animator.StringToHash("Base Layer.Standing@loop") && animator.GetCurrentAnimatorStateInfo(0).fullPathHash != oldState.fullPathHash)
        //    {
        //        AnimationChange("Next");

        //    }
        //}
        //else
        //{
        //    if (animator.GetCurrentAnimatorStateInfo(0).fullPathHash == Animator.StringToHash("Base Layer.Walking@loop") && animator.GetCurrentAnimatorStateInfo(0).fullPathHash != oldState.fullPathHash)
        //    {
        //        AnimationChange("Back");
        //    }


        //}
    }
    ////関数にまとめる予定
    //void AnimationChange(string stateName)
    //{
    //    animator.SetTrigger(stateName);
    //    oldState = animator.GetCurrentAnimatorStateInfo(0);
    //}

    public void HitDamage(GameObject enemy)
    {
        if (state == State.Damage && state == (State)animation_state)
        {
            animator.Play("Damage", 0, 0.2f);
            source.PlayOneShot(DamageSE[Random.Range(0,2)]);
        }
        else
        {
            state = State.Damage;
            animator.SetInteger("State", (int)state);
            animator.Play("Damage", 0, 0.0f);
            ScoreManager.SubScore(1500);
            source.PlayOneShot(DamageSE[Random.Range(0, 2)]);
        }
        damageTime = 0.0f;
        animator.speed = 1.0f;
        Move = -(enemy.transform.position - transform.position) / 5;
        Move.y = 0;
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.transform.tag == "Enemy")
        {
            if (mutekiTime > 0.0f)
                return;

            //スライム・トロールはガード可能
            if (col.transform.parent.GetComponent<EnemyTestScript>().type <= 1)
            {
                if (state == State.Guard)
                {

                    return;
                }
                if (state == State.Atack)
                {
                    return;
                }
            }
            damageCount++;
            if (damageCount < 2)
            {
                HitDamage(col.gameObject);
            }
        }
        if (col.transform.tag == "EnemyAtack")
        {
            if (mutekiTime > 0.0f)
                return;
            ////スライム・トロールはガード可能
            //if (col.GetComponent<EnemyTestScript>().type <= 1)
            //{
            //    if (state == State.Guard)
            //    {
            //        return;
            //    }
            //}
            damageCount++;
            if (damageCount < 2)
            {
                HitDamage(col.gameObject);
            }
        }
        if (col.transform.tag == "EnemyBullet")
        {
            if (mutekiTime > 0.0f)
                return;
            if (state != State.Guard && state != State.Atack)
            {
                damageCount++;
                if (damageCount < 2)
                {
                    HitDamage(col.gameObject);
                }
            }
        }
    }

}
