﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

public class TitleMenuScript : MonoBehaviour {

	GameObject[] menu = new GameObject[3];
	public int target = 0;
	// Use this for initialization
	void Start () {
		for(int i = 0; i < menu.Length;i++){
			menu [i] = transform.GetChild(i).gameObject;
			menu [i].GetComponent<RectTransform>().anchoredPosition = new Vector2 (0, i * -50);
		}
		ChangeTarget (0);
	}
	
	// Update is called once per frame
	void Update () {
		if (Inputs.move.z != 0) {
			ChangeTarget ((int)Mathf.Sign(Inputs.move.z));

		}
		if (Inputs.Jump) {
			
		}

	}

	void ChangeTarget(int direction){
		target = Mathf.Clamp(target + direction,0,menu.Length-1);
		for(int buttonNum = 0; buttonNum < menu.Length; buttonNum++){
			if (buttonNum == target) {
				menu [buttonNum].GetComponent<Button> ().interactable = true;
			} else {
				menu [buttonNum].GetComponent<Button> ().interactable = false;
			}
		}
			
	}
}
