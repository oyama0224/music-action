﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class R_IconScript : MonoBehaviour {

    Vector2 baseSize;
    Image image;
    Image ScoreBack;
    Vector2 size;
    Vector3 color;
	// Use this for initialization
	void Start () {
        image = GetComponent<Image>();
        ScoreBack = GameObject.Find("ScoreBack").GetComponent<Image>();
        baseSize = image.rectTransform.sizeDelta;
        
	}
	
	// Update is called once per frame
	void Update () {
        
        float BPM = MusicController.GetBPM(true) /150.0f;
        if (BPM != 0)
        {
            size = Vector3.Lerp(size,baseSize * BPM,50 * Time.deltaTime);
            image.rectTransform.sizeDelta = size;
            color = Vector3.Lerp(color, MusicController.addFFT, 0.1f);
            image.color = new Color (color.x,color.y,color.z,0.3f);
            ScoreBack.color = new Color(color.x, color.y, color.z,0.3f);
        }
        else
        {
            if (size.magnitude > baseSize.magnitude / 100)
            {
                size = Vector3.Lerp(size,size * 0.9f,50 * Time.deltaTime);
                image.rectTransform.sizeDelta = size;
                image.color = new Color(color.x, color.y, color.z, 0.3f);
                ScoreBack.color = new Color(color.x, color.y, color.z, 0.3f);
            }
        }
	}
}
