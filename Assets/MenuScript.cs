﻿using UnityEngine;
using System.Collections;

public class MenuScript : MonoBehaviour {

    bool isLoad;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (FadeScript.isFade)
            return;
        if (Inputs.Jump && !isLoad)
        {
            isLoad = true;
            GameObject.Find("GameSystem").GetComponent<FadeScript>().StartFade(0,"");
        }
	}
}
