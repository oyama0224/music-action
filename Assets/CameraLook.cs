﻿using UnityEngine;
using System.Collections;

public class CameraLook : MonoBehaviour
{

    Vector2 m_cameramove;
    Vector2 m_sensitive = new Vector2(1.0f,15.0f);
    Vector2 m_beforemousePos;
    Vector2 m_screenSize;
    Vector2 m_mouseLimits;
    Vector2 m_mouseArea = new Vector2(100, 80);
    PlayerControll playercontroll;
    CameraScript camerascript;
    // Use this for initialization
    void Start()
    {
        playercontroll = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerControll>();
        camerascript = GameObject.FindWithTag("MainCamera").GetComponent<CameraScript>();
        SetScreenSize();
    }
    void SetScreenSize()
    {
        m_screenSize = new Vector2(Screen.width, Screen.height);
        m_mouseLimits = m_screenSize;
    }
    // Update is called once per frame
    
    void Update()
    {

        m_cameramove = Inputs.cameramove;



        camerascript.AddAngleY((m_cameramove.x) * m_sensitive.x);


        
        camerascript.AddAngleX(-(m_cameramove.y) * m_sensitive.y);

    }
}
