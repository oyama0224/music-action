﻿using UnityEngine;
using System.Collections;

public class EnemyHit : MonoBehaviour
{
    GameObject destroy_effect;
    int hitCount;
    public int DestroyCount = 1;

    // Use this for initialization
    void Start()
    {
       // destroy_effect = Resources.Load<GameObject>("Effect/DestroyEffect");
                destroy_effect = Resources.Load<GameObject>("OnpuEffect2");
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnDestroy()
    {
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "Weapon")
        {
            if (transform.parent.GetComponent<EnemyTestScript>().ismuteki)
            {
                return;
            }


            if (collider.transform.parent.GetComponent<WeaponControll>().isAtack)
            {
                transform.parent.GetComponent<EnemyTestScript>().startDestroyMotion(true);
                GameObject effect = GameObject.Instantiate(destroy_effect);
                if (transform.parent.GetComponent<EnemyTestScript>().type != 2)
                    GetComponent<MeshCollider>().enabled = false;
                else
                {
                    SphereCollider[] temp = GetComponents<SphereCollider>();
                    foreach (SphereCollider sc in temp)
                    {
                        sc.enabled = false;
                    }

                }
                Destroy(transform.parent.gameObject,0.5f);
                this.enabled = false;
            }
            return;

        }
        else if (collider.tag == "Bullet")
        {
            
            if (transform.parent.GetComponent<EnemyTestScript>().ismuteki)
            {
                return;
            }

            if (collider.GetComponent<BulletScript>() != null)
            {
                hitCount++;
                if (hitCount >= DestroyCount)
                {
                    if(transform.parent.GetComponent<EnemyTestScript>().type != 2)
                        GetComponent<MeshCollider>().enabled = false;
                    else
                    {
                        SphereCollider[] temp = GetComponents<SphereCollider>();
                        foreach (SphereCollider sc in temp)
                        {
                            sc.enabled = false;
                        }

                    }
                    transform.parent.GetComponent<EnemyTestScript>().startDestroyMotion();
                    transform.parent.parent = collider.transform;
                    GameObject effect = GameObject.Instantiate(destroy_effect);
                    effect.transform.position = collider.transform.position;
                    //effect.transform.parent = collider.transform;
                    this.enabled = false;
                }
                else
                {
                    transform.parent.GetComponent<EnemyTestScript>().SetDamageColor();
                }

            }
            return;
        }
        else if (collider.tag == "Player")
        {
            //if (!collider.GetComponent<WeaponControll>().isAtack && !collider.GetComponent<WeaponControll>().isGuard)
            //{
            //    collider.GetComponent<PlayerControll>().HitDamage(gameObject);
            //}
            //if (collider.GetComponent<WeaponControll>().isGuard)
            //{

            //}
        }

    }
}
