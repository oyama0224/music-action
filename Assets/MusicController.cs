﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;



public class MusicController : MonoBehaviour
{
    FadeScript fadescript;

    static AudioSource audioSource;
    float[] spectrum;
    float[] volume;
    float[] addvolume;
    int precision = 1;
    const float threshold = 0.00f;

    public struct FileProperty
    {
        public string title;
        public string artist;
    };


    public static MusicController instance;
    //float[] allSamples;

    public static bool inMusic;

    static bool[] timing;//敵出現
    static bool[] timing_Hard;
    static bool[] timing2;
    static int[] bpm;
    static int[] bpm_withtiming;
    static bool isStart;
    static int before_sampleCount;
    AudioClip clip;
    static float[] allSamples;
    float maxV;
    float maxN;

    int maxspectrum;
    int minspectrum;

    float size;

    float beforCount;
    float overCount;

    float[] lowRateArrays;
    float[] midRateArrays;
    float[] highRateArrays;

    public static float lowRate;
    public static float midRate;
    public static float highRate;

    static float lowRate_temp;
    static float midRate_temp;
    static float highRate_temp;
    static int addFFTCount;

    public static Vector3 beforeFFT;
    public static Vector3 nowFFT;
    public static Vector3 addFFT;

    public static Vector3 beforeRate;

    static FileProperty fileproperty;

    static WeaponControll weapon_script;

    static float FFTCount;

    int spawnCount;

    int now_mod;
    int now_oct;
    int rism;

    public static int Count4;
    public static int Count3;
    public static int allSamplesCount;

    float averageVol;
    //敵スポーンテスト
    static GameObject enemy;

    public static int GameLevel;

    static float offsetvol;

    public static FileProperty Property
    {
        set
        {
            fileproperty = value;
        }
        get
        {
            return fileproperty;
        }
    }

    void InitParam()
    {
        beforCount = 3.0f;
        overCount = 0.0f;
        before_sampleCount = 0;
        spawnCount = 0;
        inMusic = false;
        Count3 = 0;
        Count4 = 0;
        allSamplesCount = 0;

        switch(GameLevel){
            case 0:
                offsetvol = 0.3f;
                break;
            case 1:
                offsetvol = 0f;
                break;
        }
        isStart = false;
    }

    // Use this for initialization
    void Start()
    {
        if (instance != null && instance != this)
        {
            Destroy(this);
        }
        if (instance == null)
        {
            Application.targetFrameRate = 200;
            instance = this;
            DontDestroyOnLoad(gameObject);
            audioSource = GetComponent<AudioSource>();
            enemy = Resources.Load<GameObject>("Enemy");
            fadescript = GetComponent<FadeScript>();

        }



        //SearchTempo4();

    }

    void SearchTempo()
    {
        clip = GetComponent<AudioSource>().clip;
        float[] allSamples = new float[clip.samples * clip.channels];
        clip.GetData(allSamples, 0);

        int count = 0;

        float max = 0;
        if (clip.channels == 2)
        {
            timing = new bool[clip.samples * clip.channels];
            for (int i = 0; i < timing.Length; i++)
            {
                if (max < Mathf.Abs(allSamples[i]))
                {
                    max = Mathf.Abs(allSamples[i]);
                }
            }

            for (int i = 0; i < timing.Length; i++)
            {
                float test = allSamples[i];
                if (max - max / 10 < Mathf.Abs(allSamples[i]))
                {

                    timing[i] = true;
                    count++;
                    continue;   //スキップ

                }
            }


        }
        return;


    }
    void SearchTempo2()
    {


        timing2 = new bool[clip.samples * clip.channels];

        int split_long = clip.frequency * 2; //間隔
        int split = clip.frequency / 10;
        int last_timing = 0;


        int count = 0;

        float low = 0;
        float mid = 0;
        float high = 0;
        spectrum = new float[1024];
        lowRateArrays = new float[clip.samples * clip.channels];
        midRateArrays = new float[clip.samples * clip.channels];
        highRateArrays = new float[clip.samples * clip.channels];
        float deltafreq = AudioSettings.outputSampleRate / 1024;

        float averagetemp = 0f;
        int tempCount = 0;
        float addVol = 0;


        for (int i = clip.channels; i < allSamples.Length; i += split)
        {

            low = 0;
            mid = 0;
            high = 0;

            audioSource.timeSamples = i / 2;
            audioSource.Play();
            audioSource.GetSpectrumData(spectrum, 0, FFTWindow.BlackmanHarris);
            for (int k = 0; k < 1024; k++)
            {
                if (k * deltafreq < 14700)//14700
                {
                    low += spectrum[k];
                }
                else if (k * deltafreq < 29400)
                {
                    mid += spectrum[k];
                }
                else if (k * deltafreq < 44100)
                {
                    high += spectrum[k];
                }



            }

            lowRateArrays[i] = (low / low + mid + high);
            midRateArrays[i] = (mid / low + mid + high) * 10;
            highRateArrays[i] = (high / low + mid + high) * 100;

            if (i == clip.channels)
            {
                lowRateArrays[0] = lowRateArrays[i];
                midRateArrays[0] = midRateArrays[i];
                highRateArrays[0] = highRateArrays[i];
            }

            addVol = midRateArrays[i];
            addVol *= addVol;
            if (addVol > 0)
            {
                averagetemp += addVol;
            }
            tempCount++;



        }
        averagetemp /= tempCount;


        for (int i = clip.channels; i < clip.samples * clip.channels; i += split)
        {

            if (i > split)
            {
                addVol = midRateArrays[i];
                addVol *= addVol;
                if (addVol > averagetemp)
                {
                    if (false)
                    {


                        //audioSource.timeSamples = j / clip.channels;
                        //audioSource.GetSpectrumData(spectrum, 0, FFTWindow.Hamming);
                        //for (int j = 0; j < 1024; j++)
                        //{
                        //    if (deltafreq * j < 14700)
                        //    {
                        //        low += spectrum[j];
                        //    }
                        //    else if (deltafreq * j < 29400)
                        //    {
                        //        mid += spectrum[j];
                        //    }
                        //    else if (deltafreq * j < 44100)
                        //    {
                        //        high += spectrum[j];
                        //    }
                        //}
                    }
                    timing2[i] = true;
                    last_timing = i;
                    count++;
                }
            }

        }


        audioSource.Stop();
        for (int i = last_timing; i > 0; i -= split)
        {
            if (i - split >= 0)
            {
                for (int j = i; j > 0; j -= split)
                {
                    if (timing2[i] && timing2[i - split])
                    {
                        timing2[i] = false;
                        count--;
                    }
                }
            }
        }
    }

    void SearchAverageVol()
    {
        
    }

    void SearchTempo3()
    {


        timing2 = new bool[clip.samples * clip.channels];

        int split_long = clip.frequency * 3; //間隔
        int split = clip.frequency / 10;
        int last_timing = 0;


        int count = 0;

        float low = 0;
        float mid = 0;
        float high = 0;
        spectrum = new float[1024];
        lowRateArrays = new float[clip.samples * clip.channels];
        midRateArrays = new float[clip.samples * clip.channels];
        highRateArrays = new float[clip.samples * clip.channels];
        float deltafreq = clip.frequency / 1024;
        for (int i = clip.channels; i < clip.channels + clip.channels; i++)
        {
            allSamples[i - clip.channels] = allSamples[i];
            //bpm[i] = 0;
        }

        for (int i = clip.channels + split_long; i < clip.samples * clip.channels; i += split_long)
        {
            float averagetemp = 0f;
            int tempCount = 0;

            for (int j = i - split_long; j < i; j += split)
            {

                //audioSource.timeSamples = j / clip.channels;
                //audioSource.GetSpectrumData(spectrum, 0, FFTWindow.Hamming);
                //for (int k = 0; k < 1024; k++)
                //{
                //    if (k * deltafreq < 14700)//14700
                //    {
                //        low += spectrum[k];
                //    }
                //    else if (k * deltafreq < 29400)
                //    {
                //        mid += spectrum[k];
                //    }
                //    else if (k * deltafreq < 44100)
                //    {
                //        high += spectrum[k];
                //    }



                //}
                //lowRateArrays[j] = (low / low + mid + high);
                //midRateArrays[j] = (mid / low + mid + high);
                //highRateArrays[j] = (high / low + mid + high);


                if (j > split)
                {
                    float addVol = allSamples[j] - allSamples[j - clip.channels];
                    addVol *= addVol;

                    if (addVol > 0f)
                    {
                        averagetemp += addVol;
                        tempCount++;
                    }
                }

            }
            averagetemp /= tempCount;


            for (int j = i - split_long; j < i; j += split)
            {
                if (j > split)
                {
                    float addVol = allSamples[j] - allSamples[j - clip.channels];
                    addVol *= addVol;
                    if (addVol > averagetemp + averagetemp * offsetvol * 4.4f)
                    {
                        if (false)
                        {
                            

                            //audioSource.timeSamples = j / clip.channels;
                            //audioSource.GetSpectrumData(spectrum, 0, FFTWindow.Hamming);
                            //for (int j = 0; j < 1024; j++)
                            //{
                            //    if (deltafreq * j < 14700)
                            //    {
                            //        low += spectrum[j];
                            //    }
                            //    else if (deltafreq * j < 29400)
                            //    {
                            //        mid += spectrum[j];
                            //    }
                            //    else if (deltafreq * j < 44100)
                            //    {
                            //        high += spectrum[j];
                            //    }
                            //}
                        }
                        timing2[j] = true;
                        last_timing = i;
                        count++;
                    }
                }

            }

        }

        Count3 = count;

        //for (int i = last_timing; i > 0; i -= split)
        //{
        //    if (i - split >= 0)
        //    {
        //        for (int j = i; j > 0; j -= split)
        //        {
        //            if (timing2[i] && timing2[i - split])
        //            {
        //                timing2[i] = false;
        //                count--;
        //            }
        //        }
        //    }
        //}
    }


    void SearchTempo4()
    {
        InitParam();

        allSamples = new float[clip.samples * clip.channels];
        timing = new bool[clip.samples * clip.channels];
        timing_Hard = new bool[clip.samples * clip.channels];
        bpm = new int[clip.samples * clip.channels];
        bpm_withtiming = new int[clip.samples * clip.channels];
        float average;
        const int split = 8000; //間隔
        int last_timing = 0;
        //Debug.Log(clip.loadState);      //エラーがきになる
        clip.GetData(allSamples, 0);
        average = 0.0f;



        int count = 0;
        float averagetemp;
        int maxCount = 0;
        float max = 0;

        float addvol;

        addvolume = new float[clip.samples * clip.channels];

        for (int i = clip.channels; i < clip.channels + clip.channels; i++)
        {
            allSamples[i - clip.channels] = allSamples[i];
        }

        for (int i = clip.channels; i < clip.samples * clip.channels; i += clip.channels)
        {
            addvol = allSamples[i] - allSamples[i - clip.channels] * allSamples[i] - allSamples[i - clip.channels];
            if (addvol > 0f)
            {
                average += addvol;
                count++;
            }
            addvolume[i] = addvol;
        }
        average /= count;
        count = 0;  //次の用意
        for (int i = clip.channels + split; i < clip.samples * clip.channels; i += split)
        {
            averagetemp = 0f;
            int tempCount = 0;
            for (int j = i - split; j < i; j += clip.channels)
            {
                addvol = allSamples[j] - allSamples[j - clip.channels] * allSamples[j] - allSamples[j - clip.channels];
                if (addvol > 0.01f)
                {
                    averagetemp += addvol;
                    tempCount++;
                    if (addvol > max)
                    {
                        max = addvol;
                        maxCount = i;//trueにする位置を記憶
                    }
                }

            }
            averagetemp /= tempCount;
            if (average  <= averagetemp)
            {
                

                {
                    timing_Hard [maxCount] = true;


                }

                timing[maxCount] = true;
                count++;
                last_timing = i;
                for (int j = i - split; j < i; j += clip.channels)
                {
                    bpm_withtiming[j] = tempCount;
                }


            }
            maxCount = 0;
            max = 0;
            for (int j = i - split; j < i; j += clip.channels)
            {
                bpm[j] = tempCount;
            }
        }

        for (int i = last_timing; i > 0; i -= split)
        {
            if (i - split >= 0)
            {
                if (timing[i] && timing[i - split])
                {
                    timing[i] = false;
                    count--;
                }
            }
        }

        averageVol = average;

        Count4 = count;
        allSamplesCount = clip.samples;

        return;
    }
    //minspectrum = 0;
    //maxspectrum = 0;
    //volume = new float[audioSource.clip.samples / 1000];
    //addvolume = new float[audioSource.clip.samples / 1000];


    //for (int i = 0; i < (audioSource.clip.samples / 1000); i++)
    //{
    //    audioSource.timeSamples = i * 1000;

    //    audioSource.GetSpectrumData(spectrum, 0, FFTWindow.BlackmanHarris);

    //    float temp = 0;

    //    for (int num = 0; num < spectrum.Length; num++)
    //    {
    //        temp += spectrum[num];
    //    }
    //    volume[i] = temp * temp;
    //    Debug.Log(volume[i]);

    //    if (i > 0)
    //    {
    //        addvolume[i] = volume[i] - volume[i - 1];
    //        if(addvolume[i] < 0)
    //        {
    //            addvolume[i] = 0;
    //        }
    //        if (addvolume[i] > addvolume[maxspectrum])
    //            maxspectrum = i;
    //        else if (addvolume[i] < addvolume[minspectrum])
    //            minspectrum = i;
    //    }
    //}
    //for (int i = 0; i < (audioSource.clip.samples / 1000); i++)
    //{
    //    if (addvolume[i] > addvolume[maxspectrum]-0.1f)
    //    {
    //        if (addvolume[i] > addvolume[i + rism]-0.1f)
    //        {
    //            rism = Mathf.Abs(i - maxspectrum);
    //            return;
    //        }
    //    }


    //}
    void SearchBPM()
    {
        float max = 0;
        int count = 0;
        for (int i = clip.channels; i < allSamples.Length; i++)
        {
            float addvolume = allSamples[i] - allSamples[i - 2] * allSamples[i] - allSamples[i - 2];
            if (max < addvolume)
            {
                max = addvolume;
            }
        }
        for (int i = clip.channels; i < allSamples.Length; i++)
        {
            float addvolume = allSamples[i] - allSamples[i - 2] * allSamples[i] - allSamples[i - 2];
            if (max / 2 < addvolume)
            {
                count++;
            }
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (!isStart)
        {
            audioSource.Stop();
            return;
        }
        if (FadeScript.isFade)
            return;
        if (beforCount >= 0.0f)
        {
            audioSource.Stop();
            beforCount -= Time.deltaTime;
            if (beforCount <= 0.0f)
            {
                audioSource.Play();
                inMusic = true;
            }
            return;
        }

        if (weapon_script == null)
        {
            if (GameObject.FindGameObjectWithTag("Player") != null)
                weapon_script = GameObject.FindGameObjectWithTag("Player").GetComponent<WeaponControll>();
        }


        int i = before_sampleCount;


        EnemyManager.timing = false;

        EnemyManager.timing_hard = false;

        RealTimeFFT();
        SetBulletColor();

        while (true)
        {

            if (i >= timing.Length - 1)
            {
                Debug.Log("stop");
                
                overCount += Time.deltaTime;
                //if (overCount > 5.0f)
                //{
                //    //SceneManager.LoadScene(0);
                //    InitParam();
                //    fadescript.StartFade(3, "");
                //}
                return;
            }



            if (timing[i])
            {
               

                    if (timing_Hard[i])
                    {
                         //60秒以上の曲では残り15秒間出現させない
                        if (i < timing.Length - 15 * clip.frequency * clip.channels && timing.Length > 60 * clip.frequency * clip.channels)
                        {
                            if (EnemyManager.GetCount() < 40)
                            {
                                spawnCount++;
                                if (spawnCount % 10 == 0)
                                {
                                    if (addvolume[i] > averageVol)
                                    {
                                        EnemyManager.SpawnEnemy(EnemyTestScript.EnemyType.Tori);
                                    }
                                    else
                                    {
                                        EnemyManager.SpawnEnemy(EnemyTestScript.EnemyType.Torol);
                                    }
                                }
                                else
                                {

                                    EnemyManager.SpawnEnemy();
                                }
                            }
                        }
                        timing_Hard[i] = false;
                        EnemyManager.timing_hard = true;
                    }
                    timing[i] = false;
                    EnemyManager.timing = true;
                    //Debug.Log("spawn");
                    //while (timing[i] == true)
                    //{
                    //    i+=1;
                    //}                
                
             
            }



            if (timing2[i])
            {
                checkRism();
            }

            if (i >= audioSource.timeSamples * clip.channels)
            {
                if (!audioSource.isPlaying)
                {
                    inMusic = false;
                    //Debug.Log("stop");
                    overCount += Time.deltaTime;
                    if (overCount > 5.0f)
                    {
                        overCount = 0.0f;
                        before_sampleCount = 0;
                        inMusic = false;
                        isStart = false;
                        fadescript.StartFade(4);
                    }
                    return;

                }

                //Debug.Log(i);
                //before_sampleCount = i+1;
                break;

            }
            else
            {
                i += clip.channels;
                continue;
            }
        }

        before_sampleCount = i;




        //audioSource.GetSpectrumData(spectrum, 0, FFTWindow.BlackmanHarris);
        //float maxV = 0.0f, maxN = 0.0f;
        //for (int i = 0; i < spectrum.Length; i++)
        //{
        //    if (spectrum[i] > maxV && spectrum[i] > threshold)
        //    {
        //        maxV = spectrum[i];
        //        maxN = i;
        //    }
        //}



        //ConvertScaleToInt(ConvertHertzToScale(maxV));

        //if (now_mod % 3 == 0)
        //{
        //    //GameObject.Instantiate(enemy);
        //}

    }

    private void SetBulletColor()
    {
        nowFFT = new Vector3(MusicController.highRate, MusicController.midRate, MusicController.lowRate);
        addFFT = nowFFT - beforeFFT;

        beforeFFT = nowFFT;

        Vector3 tempFFTAdd = addFFT;

        tempFFTAdd /= 10;
        if (tempFFTAdd.x < 0) tempFFTAdd.x = 0;
        if (tempFFTAdd.y < 0) tempFFTAdd.y = 0;
        if (tempFFTAdd.z < 0) tempFFTAdd.z = 0;

        //Debug.Log(tempFFTAdd);

        if (tempFFTAdd.x > 1 || tempFFTAdd.y > 1 || tempFFTAdd.z > 1)
            tempFFTAdd = tempFFTAdd.normalized / 2;



        BulletScript.color = new Color(tempFFTAdd.x, tempFFTAdd.y, tempFFTAdd.z);
    }

    void RealTimeFFT()
    {
        float low = 0;
        float mid = 0;
        float high = 0;

        //FFTCount += Time.deltaTime;

        float deltafreq = AudioSettings.outputSampleRate / 1024;

        audioSource.GetSpectrumData(spectrum, 0, FFTWindow.Hamming);
        for (int k = 0; k < 1024; k++)
        {
            if (k * deltafreq < 14700)//14700
            {
                low += spectrum[k];
            }
            else if (k * deltafreq < 29400)
            {
                mid += spectrum[k] * 10;
            }
            else if (k * deltafreq < 44100)
            {
                high += spectrum[k] * 100;
            }



        }

        lowRate = (low / low + mid + high);
        midRate = (mid / low + mid + high);
        highRate = (high / low + mid + high);

        //テストコード
        //lowRate_temp += (low / low + mid + high);
        //midRate_temp += (mid / low + mid + high);
        //highRate_temp += (high / low + mid + high);
        //addFFTCount++;

        //if (FFTCount > 0.1f)
        //{
        //    FFTCount -= 0.1f;

        //    lowRate = lowRate_temp / addFFTCount;
        //    midRate = midRate_temp / addFFTCount;
        //    highRate = highRate_temp / addFFTCount;

        //    addFFTCount = 0;

        //    lowRate_temp = 0.0f;
        //    midRate_temp = 0.0f;
        //    highRate_temp = 0.0f;


        //}

    }



    static public int GetBPM(bool withTiming = false)
    {
        if (audioSource.timeSamples * audioSource.clip.channels >= bpm.Length || !isStart)
            return 0;
        else
        {
            if (withTiming)
                return bpm_withtiming[audioSource.timeSamples * audioSource.clip.channels];
            else
                return bpm[audioSource.timeSamples * audioSource.clip.channels];
        }
    }
    static public float GetVolume()
    {
        return Mathf.Abs(allSamples[audioSource.timeSamples] * audioSource.clip.channels);
    }
    void checkRism()
    {
        weapon_script.Shot();
    }

    public void setAudioClip(AudioClip a)
    {
        StartCoroutine(loadAudioClip(a));
    }

    IEnumerator loadAudioClip(AudioClip a)
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.clip = a;
        audioSource.loop = false;
        while (audioSource.clip.loadState == AudioDataLoadState.Loading)
        {
            yield return new WaitForEndOfFrame();
        }
        Debug.Log(audioSource.clip.loadState);  //エラーがきになる
        clip = audioSource.clip;
        clip.LoadAudioData();

        SearchTempo4();
        SearchTempo3();

        audioSource.timeSamples = 0;//開始位置に

        isStart = true;

        if (SceneManager.GetActiveScene().buildIndex != 1)
        {
            fadescript.StartFade(2, "loading");

        }
        //        audioSource.Play();
    }

    float ConvertHertzToScale(float hertz)
    {
        if (hertz == 0) return 0;
        else return (12.0f * Mathf.Log(hertz / 110.0f) / Mathf.Log(2.0f));
    }
    void ConvertScaleToInt(float scale)
    {
        // 今の場合だと、mod24が0ならA、1ならAとA#の間、2ならA#…
        int s = (int)scale;
        if (scale - s >= 0.5) s += 1; // 四捨五入
        s *= precision;

        now_mod = s % (12 * precision); // 音階
        now_oct = s / (12 * precision); // オクターブ



        //
        //0A1A#2B3C4C#5D6D#7E8F9F#10G11G#
        //
        //
        //
        //
        //
        //
        //
        //
    }


}

