﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class FadeScript : MonoBehaviour {

    static Texture2D texture;
    static GUIText text;
    static float count;
    static float alpha;
    const float fadeintime = 1.0f;
    const float fadewaittime = 1.0f;
    const float fadeouttime = 2.0f;
    static bool isfadeflag;
    public static bool isFade
    {
        get
        {
            return isfadeflag;
        }
    }

	// Use this for initialization
	void Start () {
        SetupTexture();
        SetupText();
        DontDestroyOnLoad(this);
	}
	
    public static void SetupTexture()
    {
        texture = new Texture2D(Screen.width, Screen.height);
        texture.SetPixel(Screen.width, Screen.height, Color.black);
        texture.Apply();
        
    }
    void SetupText()
    {
        text = GetComponent<GUIText>();
        text.transform.Translate(new Vector3(0.5f, 0.5f));
        text.anchor = TextAnchor.MiddleCenter;
        text.alignment = TextAlignment.Center;
        text.enabled = false;

    }

	// Update is called once per frame
	void Update () {
	    //if(count > 0.0f)
     //   {
     //       count -= Time.deltaTime;
     //   }

	}
    void OnGUI()
    {
        GUI.color = new Color(0, 0, 0, alpha);
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), texture);
        //text.color = new Color(1, 1, 1, alpha);
    }

    public void StartFade(int scenenum, string songtext = null)
    {
        Fade(scenenum, songtext);
    }
    void Fade(int scenenum, string songtext)
    {
        StartCoroutine(FadeCoroutine(scenenum,songtext));
    }
    IEnumerator FadeCoroutine(int scenenum,string songtext)
    {
        count = 0.0f;

        string property_text = MusicController.Property.title + '\n' + MusicController.Property.artist;

        text.text = property_text;
        isfadeflag = true;
        if(songtext != null)
        {
            text.enabled = true;
            text.color = new Color(text.color.r, text.color.g, text.color.b, 1.0f);
            //text.text = songtext;
        }
        while (count < fadeintime)
        {
            alpha = count / fadeintime;
            count += Time.deltaTime;
            yield return 0;
        }
        alpha = 1.0f;
        count = fadewaittime;
        SceneManager.LoadScene(scenenum);
        while(count > 0.0f)
        {
            count -= Time.deltaTime;
            yield return 0;
        }
        count = fadeouttime;
        while (count > 0.0f)
        {
            alpha = count / fadeouttime;
            count -= Time.deltaTime;
            yield return 0;
        }
        alpha = 0.0f;
        isfadeflag = false;
        count = 0.0f;
        //count = 2.0f;
        //while (count > 0.0f)
        //{
        //    text.color = new Color(text.color.r,text.color.g,text.color.b, count / 2.0f);
        //    count -= Time.deltaTime;
        //}
        text.text = "";
        text.enabled = false;
        alpha = 0.0f;

    }
}
