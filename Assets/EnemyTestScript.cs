﻿using UnityEngine;
using System.Collections;


public class EnemyTestScript : MonoBehaviour
{

    static AudioClip[] SE;
    string sePath = "BGM/";

    AudioSource source;

    GameObject onpuEffect;

    bool timing;
    public enum EnemyType
    {
        Slime,
        Torol,
        Kangaru,
        Tori
    };

    public EnemyType Type
    {
        get
        {
            return enemytype;
        }
    }


    public enum State
    {
        Stay,
        Move,
        Atack,
        Destroy
    };
    public State state;
    public int animation_state;
    public int type;
    NavMeshAgent agent;
    GameObject player;
    Animator modelAnimator;
    EnemyType enemytype = EnemyType.Slime;

    float animationTime;
    float mutekiTime = 1.0f;

    GameObject kangaru_bullet;
    
    static GameObject effect;

    Color BeforeColor;

    float timer;

    public CapsuleCollider[] atackCollider;    //めんどいからエディタで設定

    public float animation_speed
    {
        set
        {
            if (modelAnimator == null)
                return;

            if (state == State.Destroy)
                return;
            switch (enemytype)
            {
                case EnemyType.Slime:
                    modelAnimator.speed = value;
                    break;
                case EnemyType.Torol:
                    if (state == State.Atack)
                        modelAnimator.speed = value + 0.1f;
                    else
                    {
                        modelAnimator.speed = value;
                    }
                    break;
                case EnemyType.Tori:
                    if (state == State.Atack)
                        modelAnimator.speed = value + 0.1f;
                    else
                    {
                        modelAnimator.speed = value;
                    }


                    break;
                default:
                    break;

            }
            //if (value > 1.0f)
            //{
            //    timing = true;
            //}else
            //{
            //    timing = false;
            //}
        }
        get
        {
            return modelAnimator.speed;
        }
    }
    public float move_speed
    {
        set
        {
            if (state == State.Destroy)
                return;
            switch (enemytype)
            {
                case (EnemyType.Slime):
                    agent.speed = value * 0.75f;
                    break;
                case (EnemyType.Torol):
                    if (state == State.Move)
                        agent.speed = value * 0.2f;
                    else
                        agent.speed = 0.0f;
                    break;
                case EnemyType.Tori:
                    if (state == State.Move)
                        agent.speed = value * 0.3f;
                    else
                    {
                        agent.speed = 0.0f;
                    }
                    break;
                default:
                    break;
            }


        }
        get
        {
            return agent.speed;
        }
    }
    public bool ismuteki
    {
        get
        {
            return mutekiTime > 0.0f;
        }
    }
    // Use this for initialization
    void Start()
    {
        enemytype = (EnemyType)type;
        if (effect == null)
        {
            effect = Resources.Load<GameObject>("Effect/SpawnEffect");
        }

        if (SE == null)
        {
            SE = new AudioClip[4];
            SE[0] = Resources.Load<AudioClip>(sePath + "SE_Slime");
            SE[1] = Resources.Load<AudioClip>(sePath + "SE_Torol");
            SE[2] = Resources.Load<AudioClip>(sePath + "SE_Kangaru");
            SE[3] = Resources.Load<AudioClip>(sePath + "SE_Tori");
        }
        source = gameObject.AddComponent<AudioSource>();
        //Debug.Log( transform.childCount);
        modelAnimator = transform.GetChild(0).GetComponent<Animator>();
        EnemyManager.SetEnemy(transform);
        GameObject.Instantiate(effect).transform.position = transform.position + new Vector3(0, 15, 0);
        kangaru_bullet = Resources.Load<GameObject>("kangaru_bullet");
        if (player == null)
            player = GameObject.FindGameObjectWithTag("Player");
        if (player != null)
            transform.LookAt(player.transform.position, Vector3.up);
        NavMeshHit closestHit;
        if (enemytype != EnemyType.Kangaru)
        {
            if (NavMesh.SamplePosition(gameObject.transform.position, out closestHit, 500f, NavMesh.AllAreas))
            {
                gameObject.transform.position = closestHit.position;
                agent = GetComponent<NavMeshAgent>();
                agent.enabled = true;
                transform.parent = GameObject.Find("EnemyManager").transform;
            }
            else
            {
                Debug.Log("a");
            }

        }
    }

    void Update()
    {
        if (state == State.Destroy)
            return;

        if (EnemyManager.timing)
        {
            timing = true;
            
        }
        else
            timing = false;
        //Updateでしかできない処理
        switch (enemytype)
        {
            case EnemyType.Torol:
                animation_state = modelAnimator.GetInteger("num");
                Animation_Torol();
                break;
            case EnemyType.Kangaru:
                
                Update_Kangaru();
                break;
            case EnemyType.Tori:
                animation_state = modelAnimator.GetInteger("num");
                Update_Tori();
                break;

        }
    }

    void Update_Kangaru()
    {
        timer += Time.deltaTime;
        if (player != null)
        {
            transform.LookAt(player.transform);
        }
        //agent.Stop();
        if (EnemyManager.timing_hard)
        {
            modelAnimator.Play("Atack",0,0.0f);
            GameObject temp = GameObject.Instantiate(kangaru_bullet);
            temp.transform.position = transform.position;
            temp.GetComponent<Rigidbody>().AddForce(transform.TransformDirection(Vector3.forward) * 3000, ForceMode.Acceleration);
        }

    }

    void Update_Tori()
    {
        float distance = Vector3.Distance(player.transform.position, transform.position);

        if (distance < 20f)
        {
            if (timing)
            {
                if (state != State.Atack)
                {
                    state = State.Atack;
                    modelAnimator.SetInteger("num", (int)state);
                    modelAnimator.Play("Atack", 0, 0.0f);
                }

            }
            else
            {
                if (state != State.Atack)
                    state = State.Stay;
            }
        }
        else
        {
            if (state != State.Atack)
                state = State.Move;
        }

        if (modelAnimator != null)
        {
            if (atackCollider != null)
            {
                bool colliderFlag = state == State.Atack;

                foreach (CapsuleCollider collider in atackCollider)
                {
                    collider.enabled = colliderFlag;
                }

            }

            switch ((State)animation_state)
            {


                case State.Move:
                    agent.Resume();
                    break;
                case State.Atack:
                    agent.Stop();
                    if (modelAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1.0f)
                    {
                        state = State.Stay;
                    }
                    break;
                case State.Stay:
                    agent.Stop();
                    modelAnimator.SetInteger("num", (int)State.Move);
                    return;
            }
            modelAnimator.SetInteger("num", (int)state);

        }
    }
    public void startDestroyMotion(bool isSord = false)
    {
        source.PlayOneShot(SE[(int)enemytype]);
        if(enemytype != EnemyType.Kangaru)
            agent.enabled = false;

        if (isSord)
        {
            GetComponent<Rigidbody>().isKinematic = false;
            GetComponent<Rigidbody>().AddForce(Vector3.up * 50.0f);
        }

        modelAnimator.speed = 2.0f;
        state = State.Destroy;
        switch (enemytype)
        {
            case EnemyType.Slime:
                 modelAnimator.SetInteger("num", (int)state);
                 ScoreManager.AddScore(300);
                 break;
            case EnemyType.Torol:
                 ScoreManager.AddScore(900);
                 break;
            case EnemyType.Kangaru:
                 ScoreManager.AddScore(3000);
                break;
            case EnemyType.Tori:
                ScoreManager.AddScore(1500);
                break;
        }
    }

    public void SetDamageColor(int count = 1)
    {
        source.PlayOneShot(SE[(int)enemytype]);
        switch (enemytype)
        {
            case EnemyType.Slime:
                BeforeColor = GetComponent<Renderer>().material.GetColor("_Color");
                GetComponent<Renderer>().material.SetColor("_Color", new Color(1, 0, 0));
                break;

            case EnemyType.Torol:
                BeforeColor = modelAnimator.transform.FindChild("pSphere3").GetComponent<Renderer>().material.GetColor("_Color");
                modelAnimator.transform.FindChild("pSphere3").GetComponent<Renderer>().material.SetColor("_Color", new Color(1, 0, 0));

            break;
        }
 
    }

    void Animation_Torol()
    {
        float distance = Vector3.Distance(player.transform.position, transform.position);

        if (distance < 6.0f)
        {
            if (timing)
            {
                if (state != State.Atack)
                {
                    state = State.Atack;
                    modelAnimator.SetInteger("num", (int)state);
                    modelAnimator.Play("Atack",0,0.0f);
                }

            }
            else
            {
                if (state != State.Atack)
                    state = State.Stay;
            }
        }
        else
        {
            if (state != State.Atack)
                state = State.Move;
        }

        if (modelAnimator != null)
        {
            switch ((State)animation_state)
            {


                case State.Move:
                    agent.Resume();
                    break;
                case State.Atack:
                    agent.Stop();
                    if (modelAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1.0f)
                    {
                        state = State.Stay;
                    }
                    break;
                case State.Stay:
                    agent.Stop();
                    break;
            }
            modelAnimator.SetInteger("num", (int)state);

        }

    }


    // Update is called once per frame
    void FixedUpdate()
    {
        if (state == State.Destroy)
            return;
        if (mutekiTime > 0.0f)
        {
            mutekiTime -= Time.fixedDeltaTime;
        }
        switch (enemytype)
        {

            case EnemyType.Slime:
                Move_Slime();
                break;
            case EnemyType.Torol:
                Move_Torol();
                break;
            case EnemyType.Tori:
                Move_Tori();
                break;

        }

    }



    void Move_Slime()
    {
        if (modelAnimator != null)
        {
            animationTime = modelAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime;
            if (agent != null)
            {
                agent.SetDestination(player.transform.position);
                if (animationTime % 1.0f < 0.2f || animationTime % 1.0f > 0.55f)
                {
                    agent.Stop();
                }
                else
                {
                    agent.Resume();
                }
            }

        }

    }
    void Move_Torol()
    {

        if (agent != null)
        {
            agent.SetDestination(player.transform.position);
        }


    }

    void Move_Tori()
    {
        if (agent != null)
        {
            agent.SetDestination(player.transform.position);
        }
    }
    public void Destroy()
    {
        if (mutekiTime <= 0.0f)
        {
            //EnemyManager.SubEnemy(transform);
            Destroy(gameObject);
        }
    }
}
